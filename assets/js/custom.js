jQuery(document).ready(function () {
    (function ($) {
        'use strict';
        // Input FirstLetter Cap
        jQuery('.firstCap, textarea').on('keypress', function (event) {
            var jQuerythis = jQuery(this),
                thisVal = jQuerythis.val(),
                FLC = thisVal.slice(0, 1).toUpperCase(),
                con = thisVal.slice(1, thisVal.length);
            jQuery(this).val(FLC + con);
        });


        //Avoid pinch zoom on iOS
        document.addEventListener('touchmove', function (event) {
            if (event.scale !== 1) {
                event.preventDefault();
            }
        }, false);
    })(jQuery)
});

jQuery(function (jQuery) {
    try {
        var menu = $('.js-item-menu');
        var sub_menu_is_showed = -1;
        for (var i = 0; i < menu.length; i++) {
            $(menu[i]).on('click', function (e) {
                e.preventDefault();
                $('.js-right-sidebar').removeClass("show-sidebar");
                if (jQuery.inArray(this, menu) == sub_menu_is_showed) {
                    $(this).toggleClass('show-dropdown');
                    sub_menu_is_showed = -1;
                } else {
                    for (var i = 0; i < menu.length; i++) {
                        $(menu[i]).removeClass("show-dropdown");
                    }
                    $(this).toggleClass('show-dropdown');
                    sub_menu_is_showed = jQuery.inArray(this, menu);
                }
            });
        }
        $(".js-item-menu, .js-dropdown").click(function (event) {
            event.stopPropagation();
        });
        $("body,html").on("click", function () {
            for (var i = 0; i < menu.length; i++) {
                menu[i].classList.remove("show-dropdown");
            }
            sub_menu_is_showed = -1;
        });
    } catch (error) {
        console.log(error);
    }
});

/* Profile-Page-Upload */
jQuery(document).ready(function () {
    var readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".file-upload").on('change', function () {
        readURL(this);
    });

    $(".upload-button").on('click', function () {
        $(".file-upload").click();
    });
});

/* Login-Page */
(function (jQuery) {
    'use strict';

    function loginTable() {
        var a = parseInt(jQuery(window).height());
        var b = parseInt(jQuery('footer').height());
        var c = a - b;
        jQuery('.login_image').css('height', c);

    }
    loginTable();

    jQuery(window).resize(loginTable);

})(jQuery);