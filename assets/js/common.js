/*=============common repeater for table============*/

$(document).ready(function () {

	jQuery(document).on('change','.editUser',function(){
		$('#editStatus').val('true');
	});

	$(document).on('change','.check-user-code',function(){
		var usercode = $(this).val();
		var label = $(this).next('.userCodeError');
		jQuery.ajax({
			method:"POST",			
			url: site_url + 'check-user-code',		
			data:{usercode:usercode},	
			dataType: "html",			
			success: function (data)			
			{				
				if(data != 0)
				{
					$('.exit-user-btn').attr('disabled',true);
					$(label).html('User code already exist');
				}
				else
				{
					$('.exit-user-btn').attr('disabled',false);
					$(label).html('');
				}
			}		
		});
	});
	
	/*======to append new field in first change append to prepend in jquery.repeater.js==========*/ 

    var repeater = $('.repeater').repeater({
		initEmpty: false,
		show: function () {
			$('.location-email').val(' ');
			$('.locationId').val('');
			$(this).slideDown();
		},
		isFirstItemUndeletable: true
	});

   var listUrl = $('.get-list').val();

    if(listUrl != undefined)
	{

    	jQuery.ajax({			
			url: site_url + listUrl,			
			dataType: "json",			
			success: function (data)			
			{				
				repeater.setList(data);
				var locationid = $('.location-tab:first').find('.location-id').val();
				if(locationid != undefined)
				{
					emailList(locationid);
				}
			}		
		});
	}
});

/*==============Common delete function===============*/

	$(document).on('click','#deleteCheckbox',function()
	{
		var attr = $(this).attr('data-attr');
		if ($(this).is(':checked')) 
		{
			if(attr == 'manage-location')
			{
				$(this).closest('li').css({ 'background-color' : 'green'});
				$(this).closest('li').addClass('removetr');
			}
			else
			{
				$(this).closest('tr').css({ 'background-color' : 'green'});
				$(this).closest('tr').addClass('removetr');
			}
		}
		else
		{
			if(attr == 'manage-location')
			{
				$(this).closest('li').css({ 'background-color' : ''});
				$(this).closest('li').removeClass('removetr');
			}
			else
			{
				$(this).closest('tr').css({ 'background-color' : ''});
				$(this).closest('tr').removeClass('removetr');
			}
			
		}
	});

 
	$(document).on('click','.delete',function(){
		var numberOfChecked = $('input:checkbox:checked').length;
		if(numberOfChecked > 0)
		{
			$('#delete-modal').modal('show');
		}
		else
		{
			toastr.error('Atleast select one item to delete', 'Error Alert', {timeOut: 2000 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
		}
	});

	$(document).on('click','.yes-delete-user',function(){
	    $('#delete-modal').modal('hide');
		var formUrl = $('.delete-url').val();
		var data = $("form").serializeArray();
		$('.preloader').css('display','block');
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$.ajax({
			url: formUrl,
			type: 'post',
			dataType: 'json',
			data: data,
			success: function(data) 
			{
				$('.preloader').css('display','none');
				if(data.success == 1)
				{
					$('.removetr').remove();
					
					toastr.success('Deleted successfully', 'Success Alert', {timeOut: 2000 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
				} 
				else
				{
					toastr.error('Not deleted successfully', 'Error Alert', {timeOut: 2000 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
				}
			}
		});
	});

	$(document).on('click','.action-btn',function(){
		var actionPerform = $(this).attr('data-attr');
		var tablename = $('.tablename').val();
		var reloadUrl = $(this).attr('data-target');
		var tableId = $('.tableid').val();
		jQuery.ajax({	
			method: "POST",		
			url: site_url + 'manage-animal-datalogs',
			data:{actionPerform:actionPerform,tablename:tablename,reloadUrl:reloadUrl,tableId:tableId},			
			dataType: "json",			
			success: function (data)			
			{				
				if(data.success == true)
				{
					if(data.url)
					{
						window.location = data.url;
					}
				}
			}		
		});
	});

/*========================trim space validation=======================*/



jQuery(document).on('click',function(){



	jQuery('body').on('keyup blur', 'input[type = "number"],input[type = "number"],input[type = "text"],input[type = "email"],input[type = "password"]', function (eve) 

	{

		if ((eve.which != 37) && (eve.which != 38) && (eve.which != 39) && (eve.which != 40)) 

		{

			var text = jQuery(this).val();

			text = text.trim();

			if (text == '') {

				jQuery(this).val('');

			}

			var string = jQuery(this).val();

			if (string != "") {

				string = string.replace(/\s+/g, " ");

				jQuery(this).val(string);

			}

		}

	});



/*====================only Alphabet validation====================*/



	jQuery(document).on('keypress', '.alphabets', function (event) 

	{

		var inputValue = event.charCode;

		if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) 

		{

			event.preventDefault();

		}

	});



/*====================Alpha Numeric validation====================*/



 	jQuery('.alphaNumeric').keyup(function() 

 	{

        if (this.value.match(/[^a-zA-Z0-9 ]/g)) {

            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');

        }

    });



/*================Numeric validation  and dot with tab working====================*/



	jQuery(document).on('keypress', '.validNumber', function (eve) 

	{

		if (eve.which == 0) {

			return true;

		}

		else

		{

			if (eve.which == '.') 

			{

				eve.preventDefault();

			}

			if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) 

			{

				if (eve.which != 8)

				{

					eve.preventDefault();

				}

			}

		}

	});



/*================only Numeric validation with tab working====================*/



	jQuery(document).on('keypress', '.onlyvalidNumber', function (eve) 

	{

		if (eve.which == 0) 

		{

			return true;

		}

		else

		{

			if ((eve.which != 46 || $(this).val().indexOf('.') == -1) && (eve.which < 48 || eve.which > 57))

			{

				if (eve.which != 8)

				{

					eve.preventDefault();

				}

			}

		}

	});

	//jQuery(".tel-number").intlTelInput();

	



		

		

/*================Check email already exist or not====================*/



	$(document).on('keyup change','.email',function(){

	var email = $(this).val();

	if(email == ''){

		$('.checkEmailValid').val('');

	}

	var tablename = $(this).attr('data-attr');

	var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

    var valid = emailReg.test(email);

	if (!valid && email != '') 

	{



		$('#emailError').html('Invalid email address');

		$('label[for="email"]').css('display','none');

		$('#emailError').css('display','block');

		$('.disabledButton').prop('disabled',true);

    } 

	else

	{

		$('#emailError').html('');

		$('#emailError').css('display','none');

		$('.disabledButton').prop('disabled',false);

        

        if($('#email').val() != '')

        {

			jQuery.ajax({

				type: "POST",

				url: site_url + 'check-email',

				data:{email:email,tablename:tablename},

				dataType: "json",

				success: function (data)

				{

					if(data == true && email != '')

					{

						

						$('#emailError').html('Email already exist.');

						$('#emailError').css('display','block');

						$('.disabledButton').prop('disabled', true);

					}

					else

					{

						$('.checkEmailValid').val('gfd');

						/*if($('.checkEmailValid').val() == true && $('.checkNoValid').val() == true){

							$('.disabledButton').prop('disabled', false);

						}*/

						

						$('#emailError').html('');

						$('#emailError').css('display','none');

					}

				},

			});

		}

	}

});



	

   	

});

