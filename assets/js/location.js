$(document).ready(function(){

    $('body').on('change','.location-name',function(){
        var locationid = $(this).parents('.location-tab').find('.location-id');
        var locationName = $(this).val();
        var name = $(this);
        if(locationid != '')
        {
            $('.preloader').css('display','block');
            $("html, body").animate({ scrollTop: 0 }, "slow");

            jQuery.ajax({
                method:"POST",           
                url: site_url + 'check-location-name',
                data: {locationName:locationName},            
                dataType: "json",           
                success: function (data)            
                { 
                    if(data == false)
                    {
                        name.val('');
                        $('.preloader').css('display','none');
                        
                        toastr.error('Location name already exist', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
                    }
                    else
                    {
                        addLocation(locationid);
                    }
                }
            });
           
        }
        
    });

    function addLocation(locationid)
    {
        $('.preloader').css('display','block');
        jQuery.ajax({
            method:"POST",           
            url: site_url + 'manage-location',
            data: $("form").serializeArray(),            
            dataType: "json",           
            success: function (data)            
            { 
                $('.preloader').css('display','none');
                if(data.success == true) 
                {
                    if(data.lastId)
                    {
                        locationid.val(data.lastId);
                        $('.locationId').val(data.lastId);
                        toastr.success('Location added successfully', 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
                    }
                    else
                    {

                        toastr.success('Location updated successfully', 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
                    
                    }
                    $('.location-email').attr('disabled',false);
                } 
                else
                {
                    toastr.error('Location not added successfully', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
                    
                }            
            }       
        }); 
    }
    
    var textArea = $('.location-email');
    var maxRows = textArea.attr('rows');
    var maxChars = textArea.attr('cols');
    textArea.keypress(function(e){
        var text = $.trim(textArea.val());
        text = text.replace(/(?:(?:\r\n|\r|\n)\s*){2}/gm, "");
        var lines = text.split('\n');
        if (e.keyCode == 13){
            if(lines.length == 10)
            {
                toastr.error('Email not enter more than 10', 'Error Alert', {timeOut: 2000 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
            }
           return lines.length < maxRows;
        }
        
    });

    $(document).on('change','.location-email',function()
    {
        $('.preloader').css('display','block');
        $("html, body").animate({ scrollTop: 0 }, "slow");
        jQuery.ajax({
            method:"POST",           
            url: site_url + 'manage-location-email',
            data: $("form").serializeArray(),            
            dataType: "json",           
            success: function (data)            
            { 
                $('.preloader').css('display','none');
                if(data.success == true)
                {
                    toastr.success(data.success_msg, 'Success Alert', {timeOut: 2000 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
                }        
                else
                {
                    toastr.error(data.success_msg, 'Error Alert', {timeOut: 2000 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
                }
            }       
        });
    });

    $(document).on('click','.location-tab',function(){

        var locationid = $(this).find('.location-id').val();
        if(locationid == '')
        {
            $('.location-email').val(' ');
        }
        else
        {
            emailList(locationid);
        }
        
    });

});

    function emailList(locationid)
    {
        if(locationid != '')
        {
            $('.preloader').css('display','block');
            
            $('.locationId').val(locationid);
            $('.location-email').attr('disabled',false);
            jQuery.ajax({
                method:"POST",           
                url: site_url + 'location-email-list',
                data: $("form").serializeArray(),            
                dataType: "html",           
                success: function (data)            
                {
                    $('.preloader').css('display','none');
                    if(data == 0)
                    {
                        $('.location-email').val(' ');
                    } 
                    else
                    {
                        $('.location-email').val(data);
                    }

                    
                }       
            });
        }
        $(document).on('click','input[data-repeater-create]',function(event){
            
        });
    }
