$(document).ready(function () {

/*================Manage Animal Sorting=====================*/

	jQuery('body').on('click', '.sorting', function () {
        var table = $(this).parents('tbody').eq(0);
        jQuery('.sorting').addClass("sorting");
        jQuery('.sorting').removeClass("sorting_desc");
        jQuery('.sorting').removeClass("sorting_asc");
        var sortBy = $(this).attr('sort_by');
        var sortType = $(this).attr('sort_type');
        
       	jQuery.ajax({
            type: "post",
            url: site_url + 'animal-list-sorting',
            data: {sortBy: sortBy, sortType: sortType},
            dataType: 'html',
            success: function (data) {
                jQuery(".allResult").html(data);
                jQuery('.sorting').each(function () {
			        if (jQuery(this).attr('sort_by') && jQuery(this).attr('sort_by') == sortBy) {
			            if (sortType == "ASC") {
			                jQuery(this).attr('sort_type', 'DESC');
			                jQuery(this).addClass('sorting_asc');
			            } else {
			                jQuery(this).attr('sort_type', 'ASC');
			                jQuery(this).addClass('sorting_desc');
			            }
			        }
			    });
            }
        });
    });

	$(document).on('click','.exit-profile',function(){
		if($('uploadImageProfile').val() == '')
		{
			$("html, body").animate({ scrollTop: 0 }, "fast");
		}
	});

/*==============Date Arrived Datepicker=================*/
	
	jQuery( ".dateArrived" ).datepicker({
		maxDate : 'today',
	    format: 'dd/mm/yyyy',
	    changeMonth: true,
        changeYear: true,
	    onSelect : function() {
	    	var editId = $('.animalEditId').val();
	    	var dt1 = $('.dobDatepicker').datepicker('getDate');
            var dt2 = $('.dateArrived').datepicker('getDate');
            if (dt2 < dt1) 
            {
            	$('.dateArrived').val('');
            	toastr.error('Date of arrived always greater than date of birth', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
		
            }
            else
            {
            	if(editId != '')
		    	{
		    		var inputVal = $('.dateArrived').val();
					var inputName = $('.dateArrived').attr('name');
					var editId = $('.animalEditId').val();
					updateAnimalProfile(inputVal,inputName,editId);
		    	}
            }
	    	
	    	if($(this).hasClass('error')){
	   	 		setTimeout(function(){ $(".dateArrived").valid(); }, 150);
	   	 	}
	    	
  	   	}
	});

/*==============Neuter Date Datepicker=================*/

	jQuery( ".neuterDate" ).datepicker({
		maxDate : 'today',
	    format: 'dd/mm/yyyy',
	    changeMonth: true,
        changeYear: true,
	    onSelect : function() {
	    	var editId = $('.animalEditId').val();
	    	var dt1 = $('.dobDatepicker').datepicker('getDate');
            var dt2 = $('.neuterDate').datepicker('getDate');
            if (dt2 < dt1) 
            {
            	$('.neuterDate').val('');
            	toastr.error('Neuter date always greater than date of birth', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
		
            }
            else
            {
            	if(editId != '')
		    	{
			    	var inputVal = $('.neuterDate').val();
					var inputName = $('.neuterDate').attr('name');
					var editId = $('.animalEditId').val();
					updateAnimalProfile(inputVal,inputName,editId);
				}
            }
            
	    	
  	   	}
	});

/*==============Date of Birth Datepicker=================*/

	jQuery( ".dobDatepicker" ).datepicker({
		onSelect: calculateage,
		maxDate : 'today',
	    format: 'dd/mm/yyyy',
	    changeMonth: true,
        changeYear: true,
	   
	});

	function calculateage() {
		var start = $('.dobDatepicker').datepicker('getDate');
	  	var end       = new Date();
	  	var age_year  = Math.floor((end - start)/31536000000);
	  	var age_month = Math.floor(((end - start)% 31536000000)/2628000000);
	  	var age_day   = Math.floor((((end - start)% 31536000000) % 2628000000)/86400000);
	  	$('.ageYear').val(age_year+' Y');
		$('.ageMonth').val(age_month+' M');
		$('.ageDay').val(age_day+' D');
		var editId = $('.animalEditId').val();
    	if(editId != '')
    	{
			var inputVal = $('.dobDatepicker').val();
			var inputName = $('.dobDatepicker').attr('name');
			var editId = $('.animalEditId').val();
			updateAnimalProfile(inputVal,inputName,editId);
		}

		$('.dateArrived').val('');
		$('.neuterDate').val('');
   		start.setDate(start.getDate());
        $('.dateArrived').datepicker('option', 'minDate', start);
        $('.neuterDate').datepicker('option', 'minDate', start);
        if($(this).hasClass('error')){
   	 		setTimeout(function(){ $(".dobDatepicker").valid(); }, 150);
   	 	}
	}

/*==============First Injection Datepicker=================*/

	jQuery( ".datepicker1" ).datepicker({
		maxDate : 'today',
	    format: 'dd/mm/yyyy',
	    autoOpen: false,
	    changeMonth: true,
        changeYear: true,
	    onSelect : function(dateValue, inst) {
	    	var editId = $('.animalEditId').val();
	    	var dt1 = $('.dobDatepicker').datepicker('getDate');
            var dt2 = $('.datepicker1').datepicker('getDate');
            if (dt2 < dt1) 
            {
            	$('.datepicker1').val('');
            	toastr.error('Injection date always greater than date of birth', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
		
            }
            else
            {
            	if(editId != '')
		    	{
			    	var inputVal = $('.datepicker1').val();
					var inputName = $('.datepicker1').attr('name');
					var editId = $('.animalEditId').val();
					updateAnimalProfile(inputVal,inputName,editId);
				}

		    	$('.datepicker2').val('');
	  	   		var date2 = $('.datepicker1').datepicker('getDate');
	            date2.setDate(date2.getDate() + 1);
	            $('.datepicker2').datepicker('option', 'minDate', date2);
            }
	    	

			if($(this).hasClass('error')){
  	   	 		setTimeout(function(){ $(".datepicker1").valid(); }, 150);
  	   	 	}
  	   	}
	});

/*==============Second  Injection Datepicker=================*/

	jQuery( ".datepicker2" ).datepicker({
		format: 'dd/mm/yyyy',
		maxDate : 'today',
		changeMonth: true,
        changeYear: true,
	    onSelect : function(dateValue) {
	    	

			var dt1 = $('.datepicker1').datepicker('getDate');
            var dt2 = $('.datepicker2').datepicker('getDate');
            //check to prevent a user from entering a date below date of dt1
            if (dt2 >= dt1) {
            	$('.datepicker2').datepicker('option', 'minDate', dateValue);
            	var editId = $('.animalEditId').val();
		    	if(editId != '')
		    	{
	                var inputVal = $('.datepicker2').val();
					var inputName = $('.datepicker2').attr('name');
					var editId = $('.animalEditId').val();
					updateAnimalProfile(inputVal,inputName,editId);
				}
            }
            else
            {
            	$('.datepicker2').val('');
            	toastr.error('Second injection date always greater than first injection', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
		
            }
            

  	   		if($(this).hasClass('error')){
  	   	 		setTimeout(function(){ $(".datepicker2").valid(); }, 150);
  	   	 	}
  	   	}
	});
});

/*=============On change animal status in animal profile change background=============*/

$(document).on('change','.animal-status',function(){
	var color = $('option:selected',this).css('background-color');
	$(this).css('backgroundColor',color);

});

/*==============Add Animal Weight on change and enter=================*/

$(document).on('keypress','.animal-weight',function()
{
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if($(this).val() != '' && keycode == '13')
	{
		addAnimalWeight();
	}
});


$(document).on('blur','.animal-weight',function()
{
	if($(this).val() != '')
	{
		addAnimalWeight();
	}
});
function addAnimalWeight()
{
	var animalId = $('.animal-id').val();
		var max = parseFloat($('.max-weight').val()).toFixed(2);
		var weight = parseFloat($('.animal-weight').val()).toFixed(2);
	    var currentdate = new Date();
	    if (currentdate.getMinutes() < parseInt(10))
	    {
	    	var currentMin = '0'+currentdate.getMinutes();
	    }
	    else
	    {
	    	var currentMin = currentdate.getMinutes();
	    }
	    if (currentdate.getHours() < parseInt(10))
	    {
	    	var currentHours = '0'+currentdate.getHours();
	    }
	    else
	    {
	    	var currentHours = currentdate.getHours();
	    }
	     if (currentdate.getSeconds() < parseInt(10))
	    {
	    	var currentSec = '0'+currentdate.getSeconds();
	    }
	    else
	    {
	    	var currentSec = currentdate.getSeconds();
	    }
	    var datetime = currentdate.getDate() + "/"
	                + (currentdate.getMonth()+1)  + "/" 
	                + currentdate.getFullYear() + " "  
	                + currentHours + ":"  
	                + currentMin + ":" 
	                + currentSec;
	    
	    if(parseFloat(weight) > parseFloat(max)) 
	    {
	    	$(".animal-weight").val('');
	    	toastr.error('Weight not more than 9.99', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
		}
	 	else
	 	{
	 		var animalWeight = weight;
	 		$(".animal-weight").val(weight);
	 		addWeight(animalWeight,datetime,animalId);
	 	}
}

/*==============OBS button functionality=================*/

$(document).on('click','.obs',function(){
	var animalId = $('.animal-id').val();
	$('.preloader').css('display','block');
	$("html, body").animate({ scrollTop: 0 }, "slow");
	$.ajax({
		url: site_url + 'add-weight-obs',
		type: 'post',
		dataType: 'json',
		data: {animalId:animalId},
		success: function(data) 
		{
			$('.preloader').css('display','none');
			if(data.success == 1)
			{
				toastr.success('OBS successfully', 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			} 
			else
			{
				toastr.error('OBS not Successfully', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			}
		}
	});
});

/*==============Add ANimal History on change and enter=================*/

$(document).on('keypress','.animal-history',function()
{
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if($(this).val() != '' && keycode == '13')
	{
		addanimalHistory();
	}
});

$(document).on('blur','.animal-history',function(){
	if($(this).val() != '')
	{
		addanimalHistory();
	}
});


/*==============Update Animal profile=================*/

$(document).on('change','.update-animal-profile',function(){
	var inputVal = $(this).val();
	var inputName = $(this).attr('name');
	var editId = $('.animalEditId').val();
	updateAnimalProfile(inputVal,inputName,editId);
});

/*==============On upload image remove validator error=================*/

$(document).on('change','#profileUploadImage',function(){
	var filename = $('#profileUploadImage').val();
    if (filename.substring(3,11) == 'fakepath') {
        filename = filename.substring(12);
    } 
    $('#uploadImageProfile').val(filename);
    $("label[for='uploadImageProfile']").css('display','none');
});

/*==============On change upload image update image=================*/

$(document).on('change', '#imageUpload', function(){
   (function(){
	   	var filepath = $("#imageUpload");
	    var type = filepath[0].files[0].type; // get length
	    if(type == 'image/png' || type == 'image/jpeg' || type != 'image/jpg')
	    {
	    	$("#formAvatar").ajaxForm({
		   dataType : 'json', 
		   success:  function(e){

		   if( e ){

		     if( e.success == true )
		     {
		     	 toastr.success('Profile updated Successfully', 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
					
		         $('#imageUpload').val('');
		        

		      } 
		      else 
		      {      
		        var error = '';
		        for($key in e.errors){
		          error += '' + e.errors[$key] + '';
		        }
		       	toastr.error('Profile not updated Successfully', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
						
		        $('#imageUpload').val('');
		      }    
		    }//<-- e
		      else {

		          
		      }
		       }//<----- SUCCESS
		    }).submit();
		}
		else
		{
			toastr.error('Only jpeg,png and jpg files are allowed', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
		
		}
	})(); 
});


$(document).on('click','.export-profile',function(){
	$('#export-animal-profile').modal('show');
});
$(document).on('change','.exportDoc',function(){
	$('#export-profile').submit();
});

/*============Add Weight function=============*/

function addWeight(animalWeight,datetime,animalId)
{
	$('.preloader').css('display','block');
	$("html, body").animate({ scrollTop: 0 }, "slow");
	$.ajax({
		url: site_url + 'add-animal-weight',
		type: 'post',
		dataType: 'json',
		data: {animalWeight:animalWeight,datetime:datetime,animalId:animalId},
		success: function(data) 
		{
			$('.preloader').css('display','none');
			if(data.success == 1)
			{

				$('.animal-weight').val('');
				if($('.table tr>td:first').text() == 'No record found...')
				{
					$('.table tr>td:first').remove();
				}
				$('table > tbody').prepend('<tr><td>'+datetime+'</td><td>-</td><td>'+animalWeight+'</td></tr>');
				toastr.success('Weight added Successfully', 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			} 
			else
			{
				toastr.error('Weight not added Successfully', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			}
		}
	});
}

/*============Add History function=============*/

function addanimalHistory()
{
	var animalId = $('.animal-id').val();
	var comment = $('.animal-history').val();
	var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
	                + (currentdate.getMonth()+1)  + "/" 
	                + currentdate.getFullYear() + " "  
	                + currentdate.getHours() + ":"  
	                + currentdate.getMinutes() + ":" 
	                + currentdate.getSeconds();
	    var date = currentdate.getDate() + "/"+ (currentdate.getMonth()+1)  + "/" + currentdate.getFullYear();
	    if (currentdate.getMinutes() < parseInt(10))
	    {
	    	var currentMin = '0'+currentdate.getMinutes();
	    }
	    else
	    {
	    	var currentMin = currentdate.getMinutes();
	    }
	    if (currentdate.getHours() < parseInt(10))
	    {
	    	var currentHours = '0'+currentdate.getHours();
	    }
	    else
	    {
	    	var currentHours = currentdate.getHours();
	    }
	    var time = currentHours + ":"  + currentMin;
	    $('.preloader').css('display','block');
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$.ajax({
		url: site_url + 'add-animal-history',
		type: 'post',
		dataType: 'json',
		data: {animalId:animalId,comment:comment,date:date,time:time},
		success: function(data) 
		{
			$('.preloader').css('display','none');
			if(data.success == 1)
			{
				$('.animal-history').val('');
				
				if($('.table tr>td:first').text() == 'No record found...')
				{
					$('.table tr>td:first').remove();
				}
				$('table > tbody').prepend('<tr><td>'+date+'</td><td>'+time+'</td><td>'+data.type+'</td><td>-</td><td>'+comment+'</td></tr>');
				toastr.success('History added successfully', 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			} 
			else
			{
				toastr.error('History not added successfully', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			}
		}
	});
}

/*==============Update animal profile function==============*/

function updateAnimalProfile(inputVal,inputName,editId)
{
	$('.preloader').css('display','block');
	$.ajax({
		url: site_url + 'update-animal-profile',
		type: 'post',
		dataType: 'json',
		data: {inputVal:inputVal,inputName:inputName,editId:editId},
		success: function(data) 
		{
			$('.preloader').css('display','none');
			if(data.success == true)
			{
				toastr.success('Animal updated Successfully', 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			} 
			else
			{
				toastr.error('Animal not updated Successfully', 'Error Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})
			}
		}
	});

}

	