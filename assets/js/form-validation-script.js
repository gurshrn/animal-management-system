jQuery(document).ready(function(){



/*===================common submit form function====================*/



	function submitHandler (form) 

	{
		$('.preloader').css('display','block');
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$.ajax({


			url: form.action,

			type: form.method,

			data: new FormData(form),

			contentType: false,       

			cache: false,             

			processData:false, 

			dataType: "json",

			beforeSend: function(){

				
			},

			success: function(response) {
				$('.preloader').css('display','none');

               	if(response.success == true)

				{

					toastr.success(response.success_message, 'Success Alert', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})

					setTimeout(

						  	function() 

						  	{

						  		if(response.userRefId)

						  		{

						  			jQuery('#userRefId').val(response.userRefId);

									$("#otpModal").modal({ backdrop: 'static',keyboard: false});

						  			jQuery('#login').modal('hide');

						  		}

						  		else if(response.otp)

						  		{

						  			jQuery('#otpModal').modal('hide');

						  			$("#login").modal({ backdrop: 'static',keyboard: false});

						  			jQuery('#pills-login-tab').attr('aria-selected',true);

									jQuery('#pills-register-tab').attr('aria-selected',false);

									jQuery('#pills-login-tab').addClass('active');

									jQuery('#pills-register-tab').removeClass('active');

									jQuery('#pills-login').addClass('active show');

									jQuery('#pills-register').removeClass('active show');

						  		}

						  		else if(response.url)

						  		{

									window.location = response.url;

						  		}

						  		

									

						  	}, 2000);

					

				}

				if(response.success == false)

				{

					toastr.error(response.success_message, 'Error!', {timeOut: 1500 ,positionClass : 'toast-top-right',progressBar: true,preventDuplicates: true})

					$('.disabledButton').prop('disabled', false);

					

				}

			}

		});

	}



/*======================user authenticate validation===================*/



	

	jQuery("#user-authenticate").validate({

		rules:

		{

			user: 

			{

				required: true,

				minlength:13,

				maxlength:13

				

			},

			profile: 

			{

				minlength:13,

				maxlength:13

				

			},

		},



		messages: 

		{

			user:

			{

				minlength:"Please enter 13 digit code",

				maxlength:"Please enter 13 digit code"

			},

			profile: 

			{

				minlength:"Please enter 13 digit code",

				maxlength:"Please enter 13 digit code"

				

			},



		},

		submitHandler: function(form) 

		{

			submitHandler(form);



		}

	});



/*================Add User validation==================*/



	jQuery("#addUser").validate({
		rules:
		{
			name: 
			{
				required: true,
			},
			user_code: 
			{
				required: true,
				minlength:13,
				maxlength:13
			},
			location_id:
			{
				required: true,
			},
		},
		messages: 
		{
			user_code:
			{
				minlength:'Please enter only 15 digits',
				maxlength:'Please enter only 15 digits'
			},
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}

	});



	jQuery("#location-manage").validate({

		rules:

		{

			name: 

			{

				required: true

			},

			

		},



		messages: 

		{

			

		},

		submitHandler: function(form) 

		{

			submitHandler(form);



		}

	});

	jQuery("#add-animal").validate({
		ignore: ['uploadImage'],
		rules:
		{
			name: 
			{
				required: true
			},
			date_birth: 
			{
				required: true
			},
			date_arrived: 
			{
				required: true
			},
			location_id: 
			{
				required: true
			},
			/*images:
			{
				required:true,
			},*/
			animal_code: 
			{
				required: true,
				minlength:13,
				maxlength:13
			},
			chip_number:
			{
				minlength:15,
				maxlength:15
			},
			pod_number:
			{
				minlength:2,
				maxlength:2
			},

		},
		messages: 
		{
			chip_number:
			{
				minlength:'Please enter only 15 digits',
				maxlength:'Please enter only 15 digits'
			},
			pod_number:
			{
				minlength:'Please enter only 2 digits',
				maxlength:'Please enter only 2 digits'
			},
			animal_code: 
			{
				minlength:'Please enter only 13 digits',
				maxlength:'Please enter only 13 digits'
			},
		},
		submitHandler: function(form) 
		{
			submitHandler(form);
		}
	});

	





	

});

