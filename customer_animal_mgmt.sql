-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 07, 2019 at 11:15 AM
-- Server version: 5.6.44
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `customer_animal_mgmt`
--

-- --------------------------------------------------------

--
-- Table structure for table `animal_history`
--

CREATE TABLE `animal_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `animal_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `animal_history`
--

INSERT INTO `animal_history` (`id`, `user_id`, `animal_id`, `comment`, `date`, `time`, `created_at`, `modified_at`, `deleted_at`) VALUES
(1, 1, 1, 'Test', '27/11/2018', '18:30', '2018-11-27 18:30:18', '2018-11-27 18:30:18', NULL),
(2, 1, 3, 'Test', '28/11/2018', '10:57', '2018-11-28 10:57:39', '2018-11-28 10:57:39', NULL),
(3, 1, 3, 'Ipoipo', '28/11/2018', '17:32', '2018-11-28 17:32:12', '2018-11-28 17:32:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `animal_profile`
--

CREATE TABLE `animal_profile` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `date_birth` varchar(255) NOT NULL,
  `ageYear` text NOT NULL,
  `ageMonth` text NOT NULL,
  `ageDay` text NOT NULL,
  `date_arrived` varchar(255) NOT NULL,
  `chip_number` varchar(255) NOT NULL,
  `pod_number` varchar(255) NOT NULL,
  `neuter_date` varchar(255) NOT NULL,
  `first_injection` varchar(255) NOT NULL,
  `second_injection` varchar(255) NOT NULL,
  `animal_code` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `modified_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `animal_profile`
--

INSERT INTO `animal_profile` (`id`, `name`, `location_id`, `status_id`, `date_birth`, `ageYear`, `ageMonth`, `ageDay`, `date_arrived`, `chip_number`, `pod_number`, `neuter_date`, `first_injection`, `second_injection`, `animal_code`, `image`, `deleted_at`, `modified_at`, `created_at`, `user_id`) VALUES
(1, 'Tuffy', 2, 1, '11/21/2018', '0', '0', '6', '11/26/2018', '454353454354354', '54', '11/15/2018', '11/21/2018', '', '7355423498765', 'dummy-image_1543323572.png', '2018-11-28 10:31:06', '2018-11-28 10:08:19', '2018-11-27 18:29:32', 1),
(2, 'Tuff', 3, 1, '11/22/2018', '0', '0', '6', '11/27/2018', '543534534543534', '54', '', '11/22/2018', '11/29/2018', '7355423498765', 'dummy-image_1543381519.png', '2018-11-28 10:40:57', '2018-11-28 10:35:41', '2018-11-28 10:35:19', 1),
(3, 'Test tuff', 1, 1, '11/14/2018', '0', '0', '14', '11/20/2018', '656463666635653', '65', '11/27/2018', '11/22/2018', '11/28/2018', '6546564564565', 'hqdefault_1543381951.jpg', NULL, '2018-11-28 11:32:04', '2018-11-28 10:42:31', 1),
(4, 'Test', 3, 1, '01/01/2019', '0 Y', '0 M', '17 D', '01/18/2019', '', '', '', '', '', '1234567890688', 'IMG_20190116_182905_1547812113.jpg', NULL, '2019-01-18 17:18:33', '2019-01-18 17:18:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `animal_status`
--

CREATE TABLE `animal_status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `animal_status`
--

INSERT INTO `animal_status` (`id`, `status_name`, `color`, `deleted_at`, `created_at`, `modified_at`, `user_id`) VALUES
(1, 'ISO', 'red', NULL, '2018-10-24 04:54:51', '2018-10-24 04:55:30', 1),
(2, 'IN', 'orange', NULL, '2018-10-24 04:55:16', '2018-10-24 04:56:06', 1),
(3, 'OUT', 'green', NULL, '2018-10-24 04:56:00', '2018-10-24 04:56:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `animal_weight`
--

CREATE TABLE `animal_weight` (
  `id` int(11) NOT NULL,
  `animal_id` int(11) NOT NULL,
  `weight` double NOT NULL,
  `created_at` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `modified_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `animal_weight`
--

INSERT INTO `animal_weight` (`id`, `animal_id`, `weight`, `created_at`, `deleted_at`, `modified_at`, `user_id`) VALUES
(1, 1, 1.5, '27/11/2018 18:30:08', NULL, '2018-11-27 18:30:08', 1),
(2, 3, 2.57, '28/11/2018 10:49:37', NULL, '2018-11-28 10:49:37', 1),
(3, 3, 1.23, '28/11/2018 10:50:54', NULL, '2018-11-28 10:50:54', 1),
(4, 3, 2.3, '28/11/2018 10:53:40', NULL, '2018-11-28 10:53:40', 1),
(5, 3, 1.2, '28/11/2018 10:54:35', NULL, '2018-11-28 10:54:35', 1),
(6, 3, 2.3, '28/11/2018 10:56:55', NULL, '2018-11-28 10:56:55', 1),
(7, 3, 2.25, '28/11/2018 17:31:36', NULL, '2018-11-28 17:31:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `location_associated_email`
--

CREATE TABLE `location_associated_email` (
  `id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `modified_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_associated_email`
--

INSERT INTO `location_associated_email` (`id`, `location_id`, `email`, `modified_at`, `deleted_at`, `created_at`, `user_id`) VALUES
(1, 1, 'Test@gmail.com', '2018-11-27 18:22:54', '2018-11-27 18:27:24', '2018-11-27 18:22:54', 1),
(2, 1, 'Test@gmail.com\r', '2018-11-27 18:23:36', '2018-11-28 11:24:28', '2018-11-27 18:23:36', 1),
(3, 1, 'test1@gmial.com\r', '2018-11-27 18:23:36', '2018-11-28 11:24:28', '2018-11-27 18:23:36', 1),
(4, 1, 'test2@gmail.com\r', '2018-11-27 18:23:36', '2018-11-28 11:24:28', '2018-11-27 18:23:36', 1),
(5, 1, 'test3@gmial.com\r', '2018-11-27 18:23:36', '2018-11-28 11:24:28', '2018-11-27 18:23:36', 1),
(6, 1, 'test4@gmail.com\r', '2018-11-27 18:23:36', '2018-11-28 11:24:28', '2018-11-27 18:23:36', 1),
(7, 1, 'test5@gmail.com\r', '2018-11-27 18:23:36', '2018-11-28 11:24:28', '2018-11-27 18:23:36', 1),
(8, 1, 'test6@gmail.com\r', '2018-11-27 18:23:36', '2018-11-28 11:24:28', '2018-11-27 18:23:36', 1),
(9, 1, 'test7@gmail.com\r', '2018-11-27 18:23:36', '2018-11-28 11:24:28', '2018-11-27 18:23:36', 1),
(10, 1, 'test8@gmail.com\r', '2018-11-27 18:23:36', '2018-11-27 18:27:24', '2018-11-27 18:23:36', 1),
(11, 1, 'test9@gmail.com', '2018-11-27 18:23:36', '2018-11-27 18:27:24', '2018-11-27 18:23:36', 1),
(12, 1, 'test9@gmail.com\r', '2018-11-27 18:23:50', '2018-11-27 18:27:24', '2018-11-27 18:23:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `location_email`
--

CREATE TABLE `location_email` (
  `id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `log_count` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `last_login_time` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `log_count`, `created_at`, `modified_at`, `last_login_time`, `deleted_at`) VALUES
(1, '110.225.245.6', 3, '2018-11-28 11:14:49', '2018-11-28 11:15:04', '2018-11-28 11:14:49', NULL),
(2, '125.25.26.214', 1, '2018-12-03 22:31:37', '2018-12-03 22:31:37', '2018-12-03 22:31:37', NULL),
(3, '122.173.201.40', 1, '2018-12-05 09:55:06', '2018-12-05 09:55:06', '2018-12-05 09:55:06', NULL),
(4, '223.180.183.210', 3, '2019-01-18 17:20:01', '2019-01-18 17:20:36', '2019-01-18 17:20:01', NULL),
(5, '223.225.151.42', 3, '2019-01-18 17:20:15', '2019-01-18 17:20:29', '2019-01-18 17:20:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `manage_location`
--

CREATE TABLE `manage_location` (
  `id` int(11) NOT NULL,
  `location_name` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `modified_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_location`
--

INSERT INTO `manage_location` (`id`, `location_name`, `deleted_at`, `modified_at`, `created_at`, `user_id`) VALUES
(1, 'Nott', '2018-11-28 11:24:28', '2018-11-28 05:00:33', '2018-11-27 12:52:34', 1),
(2, 'Testst', '2018-11-28 04:59:28', '2018-11-28 04:59:28', '2018-11-28 04:38:11', 1),
(3, 'Test', NULL, '2018-11-28 05:00:33', '2018-11-28 04:54:36', 1),
(4, 'Test', '2018-11-28 17:26:28', '2018-11-28 05:00:33', '2018-11-28 05:00:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `master_datalog`
--

CREATE TABLE `master_datalog` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  `table_id` int(11) NOT NULL,
  `datalog-type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_datalog`
--

INSERT INTO `master_datalog` (`id`, `user_id`, `action`, `table_name`, `table_id`, `datalog-type`, `created_at`) VALUES
(1, 1, 'Exit master datalog', '', 0, '', '2018-11-28 04:48:28'),
(2, 1, 'Master Datalog', '', 0, '', '2018-11-28 04:48:29'),
(3, 1, 'Exit master datalog', '', 0, '', '2018-11-28 04:48:35'),
(4, 1, 'Manage animal', '', 0, '', '2018-11-28 04:48:37'),
(5, 1, 'Master Datalog', '', 0, '', '2018-11-28 04:48:44'),
(6, 1, 'Exit master datalog', '', 0, '', '2018-11-28 04:49:31'),
(7, 1, 'Manage User', '', 0, '', '2018-11-28 04:49:44'),
(8, 1, 'Update user and exit manage user', '', 0, '', '2018-11-28 04:49:55'),
(9, 1, 'Master Datalog', '', 0, '', '2018-11-28 04:49:58'),
(10, 1, 'Exit master datalog', '', 0, '', '2018-11-28 04:50:15'),
(11, 1, 'Manage User', '', 0, '', '2018-11-28 04:50:17'),
(12, 1, 'Delete user', '', 0, '', '2018-11-28 04:50:20'),
(13, 1, 'Exit manage user', '', 0, '', '2018-11-28 04:50:20'),
(14, 1, 'Master Datalog', '', 0, '', '2018-11-28 04:50:25'),
(15, 1, 'Exit master datalog', '', 0, '', '2018-11-28 04:53:57'),
(16, 1, 'Manage User', '', 0, '', '2018-11-28 04:53:59'),
(17, 1, 'Exit manage user', '', 0, '', '2018-11-28 04:54:25'),
(18, 1, 'Manage Location', '', 0, '', '2018-11-28 04:54:29'),
(19, 1, 'Exit manage location', '', 0, '', '2018-11-28 04:54:39'),
(20, 1, 'Master Datalog', '', 0, '', '2018-11-28 04:54:40'),
(21, 1, 'Exit master datalog', '', 0, '', '2018-11-28 04:57:21'),
(22, 1, 'Manage Location', '', 0, '', '2018-11-28 04:57:23'),
(23, 1, 'Update location', '', 0, '', '2018-11-28 04:57:28'),
(24, 1, 'Update location', '', 0, '', '2018-11-28 04:57:28'),
(25, 1, 'Update location', '', 0, '', '2018-11-28 04:57:29'),
(26, 1, 'Exit manage location', '', 0, '', '2018-11-28 04:57:30'),
(27, 1, 'Master Datalog', '', 0, '', '2018-11-28 04:57:32'),
(28, 1, 'Exit master datalog', '', 0, '', '2018-11-28 04:58:50'),
(29, 1, 'Manage Location', '', 0, '', '2018-11-28 04:58:56'),
(30, 1, 'Delete location', '', 0, '', '2018-11-28 05:00:41'),
(31, 1, 'Master Datalog', '', 0, '', '2018-11-28 05:00:46'),
(32, 1, 'Exit master datalog', '', 0, '', '2018-11-28 05:00:56'),
(33, 1, 'Manage animal', '', 0, '', '2018-11-28 05:00:57'),
(34, 1, 'Master Datalog', '', 0, '', '2018-11-28 05:01:41'),
(35, 1, 'Exit master datalog', '', 0, '', '2018-11-28 05:01:53'),
(36, 1, 'Manage animal', '', 0, '', '2018-11-28 05:01:55'),
(37, 1, 'Manage Location', '', 0, '', '2018-11-28 05:02:19'),
(38, 1, 'Exit manage location', '', 0, '', '2018-11-28 05:04:06'),
(39, 1, 'Manage animal', '', 0, '', '2018-11-28 05:04:17'),
(40, 1, 'Exit manage animal', '', 0, '', '2018-11-28 05:04:18'),
(41, 1, 'Master Datalog', '', 0, '', '2018-11-28 05:04:20'),
(42, 1, 'Exit master datalog', '', 0, '', '2018-11-28 05:04:24'),
(43, 1, 'Create New Animal', '', 0, '', '2018-11-28 05:04:33'),
(44, 1, 'Master Datalog', '', 0, '', '2018-11-28 05:05:22'),
(45, 1, 'Exit master datalog', '', 0, '', '2018-11-28 05:05:27'),
(46, 1, 'Manage animal', '', 0, '', '2018-11-28 05:05:31'),
(47, 1, 'Edit Animal Animal_code', 'animal_profile', 2, 'animal', '2018-11-28 05:05:41'),
(48, 1, 'Exit update animal profile', 'animal_profile', 2, 'animal', '2018-11-28 05:05:43'),
(49, 1, 'Master Datalog', '', 0, '', '2018-11-28 05:05:44'),
(50, 1, 'Exit master datalog', '', 0, '', '2018-11-28 05:05:55'),
(51, 1, 'Manage animal', '', 0, '', '2018-11-28 05:05:57'),
(52, 1, 'Edit animal', '', 0, '', '2018-11-28 05:06:49'),
(53, 1, 'Exit update animal profile', 'animal_profile', 2, 'animal', '2018-11-28 05:06:52'),
(54, 1, 'Master Datalog', '', 0, '', '2018-11-28 05:06:54'),
(55, 1, 'Exit master datalog', '', 0, '', '2018-11-28 05:06:57'),
(56, 1, 'Manage animal', '', 0, '', '2018-11-28 05:07:01'),
(57, 1, 'Edit animal', '', 0, '', '2018-11-28 05:07:02'),
(58, 1, 'Viewed animal datalog', 'animal_profile', 2, 'animal', '2018-11-28 05:07:13'),
(59, 1, 'Exit animal datalog', 'animal_profile', 2, 'animal', '2018-11-28 05:08:32'),
(60, 1, 'Exit update animal profile', 'animal_profile', 2, 'animal', '2018-11-28 05:08:34'),
(61, 1, 'Manage animal', '', 0, '', '2018-11-28 05:08:36'),
(62, 1, 'Viewed animal datalog', 'animal_profile', 2, 'animal', '2018-11-28 05:08:39'),
(63, 1, 'Exit animal datalog', 'animal_profile', 2, 'animal', '2018-11-28 05:08:56'),
(64, 1, 'Exit update animal profile', 'animal_profile', 2, 'animal', '2018-11-28 05:10:52'),
(65, 1, 'Manage animal', '', 0, '', '2018-11-28 05:10:54'),
(66, 1, 'Delete animal', 'animal_profile', 0, 'animal', '2018-11-28 05:10:57'),
(67, 1, 'Exit manage animal', '', 0, '', '2018-11-28 05:10:58'),
(68, 1, 'Master Datalog', '', 0, '', '2018-11-28 05:11:00'),
(69, 1, 'Exit master datalog', '', 0, '', '2018-11-28 05:11:09'),
(70, 1, 'Manage animal', '', 0, '', '2018-11-28 05:11:10'),
(71, 1, 'Exit manage animal', '', 0, '', '2018-11-28 05:11:12'),
(72, 1, 'Manage animal', '', 0, '', '2018-11-28 05:12:01'),
(73, 1, 'Exit manage animal', '', 0, '', '2018-11-28 05:12:01'),
(74, 1, 'Master Datalog', '', 0, '', '2018-11-28 05:12:03'),
(75, 1, 'Exit master datalog', '', 0, '', '2018-11-28 05:12:07'),
(76, 1, 'Create New Animal', '', 0, '', '2018-11-28 05:12:08'),
(77, 1, 'Manage animal', '', 0, '', '2018-11-28 05:12:35'),
(78, 1, 'Viewed animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 05:12:37'),
(79, 1, 'Exit animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 05:13:00'),
(80, 1, 'Edit Animal Date_birth', 'animal_profile', 3, 'animal', '2018-11-28 05:14:25'),
(81, 1, 'Edit Animal Date_birth', 'animal_profile', 3, 'animal', '2018-11-28 05:15:33'),
(82, 1, 'Edit Animal Date_birth', 'animal_profile', 3, 'animal', '2018-11-28 05:17:18'),
(83, 1, 'Edit Animal Date_birth', 'animal_profile', 3, 'animal', '2018-11-28 05:17:22'),
(84, 1, 'Edit Animal First_injection', 'animal_profile', 3, 'animal', '2018-11-28 05:18:40'),
(85, 1, 'Edit Animal Second_injection', 'animal_profile', 3, 'animal', '2018-11-28 05:18:45'),
(86, 1, 'Viewed animal weight', 'animal_profile', 3, 'animal', '2018-11-28 05:18:53'),
(87, 1, 'Exit animal weight', 'animal_profile', 3, 'animal', '2018-11-28 05:21:01'),
(88, 1, 'Viewed animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 05:21:03'),
(89, 1, 'Exit animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 05:23:30'),
(90, 1, 'Viewed animal history', 'animal_profile', 3, 'animal', '2018-11-28 05:23:33'),
(91, 1, 'Exit animal history', 'animal_profile', 3, 'animal', '2018-11-28 05:23:35'),
(92, 1, 'Viewed animal weight', 'animal_profile', 3, 'animal', '2018-11-28 05:23:36'),
(94, 1, 'Delete animal', 'animal_profile', 3, 'animal', '2018-11-28 05:24:35'),
(95, 1, 'Exit animal weight', 'animal_profile', 3, 'animal', '2018-11-28 05:24:37'),
(96, 1, 'Viewed animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 05:24:39'),
(97, 1, 'Exit animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 05:25:01'),
(98, 1, 'Viewed animal weight', 'animal_profile', 3, 'animal', '2018-11-28 05:25:03'),
(99, 1, 'Add animal weight', 'animal_profile', 3, 'animal', '2018-11-28 05:26:55'),
(101, 1, 'Click on OBS', 'animal_profile', 3, 'animal', '2018-11-28 05:27:03'),
(102, 1, 'Click on OBS', 'animal_profile', 3, 'animal', '2018-11-28 05:27:27'),
(103, 1, 'Exit animal weight', 'animal_profile', 3, 'animal', '2018-11-28 05:27:34'),
(104, 1, 'Viewed animal history', 'animal_profile', 3, 'animal', '2018-11-28 05:27:35'),
(105, 1, 'Add animal history', 'animal_profile', 3, 'animal', '2018-11-28 05:27:39'),
(106, 1, 'Exit animal history', 'animal_profile', 3, 'animal', '2018-11-28 05:27:49'),
(107, 1, 'Viewed animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 05:27:54'),
(108, 1, 'Exit animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 05:27:59'),
(109, 1, 'Edit Animal Name', 'animal_profile', 3, 'animal', '2018-11-28 05:28:06'),
(110, 1, 'Viewed animal weight', 'animal_profile', 3, 'animal', '2018-11-28 05:28:43'),
(115, 1, 'Export animal weight', 'animal_profile', 3, 'animal', '2018-11-28 05:30:03'),
(116, 1, 'Exit animal weight', 'animal_profile', 3, 'animal', '2018-11-28 05:30:08'),
(117, 1, 'Exit update animal profile', 'animal_profile', 3, 'animal', '2018-11-28 05:30:12'),
(118, 1, 'Master Datalog', '', 0, '', '2018-11-28 05:30:22'),
(119, 1, 'Exit master datalog', '', 0, '', '2018-11-28 05:30:24'),
(120, 1, 'Invalid ip login', '', 0, '', '2018-11-28 11:15:10'),
(121, 1, 'Login successfully', 'user', 1, '', '2018-11-28 11:21:00'),
(122, 1, 'Create New Animal', '', 0, '', '2018-11-28 11:21:07'),
(123, 1, 'Manage User', '', 0, '', '2018-11-28 11:23:53'),
(124, 1, 'Manage Location', '', 0, '', '2018-11-28 11:24:18'),
(125, 1, 'Delete location', '', 0, '', '2018-11-28 11:24:28'),
(126, 1, 'Manage animal', '', 0, '', '2018-11-28 11:24:35'),
(127, 1, 'Exit manage animal', '', 0, '', '2018-11-28 11:24:52'),
(128, 1, 'Master Datalog', '', 0, '', '2018-11-28 11:24:55'),
(129, 1, 'Export master datalog', '', 0, '', '2018-11-28 11:24:57'),
(130, 1, 'Manage Location', '', 0, '', '2018-11-28 11:25:12'),
(131, 1, 'Manage animal', '', 0, '', '2018-11-28 11:25:22'),
(132, 1, 'Viewed animal weight', 'animal_profile', 3, 'animal', '2018-11-28 11:25:54'),
(133, 1, 'Click on OBS', 'animal_profile', 3, 'animal', '2018-11-28 11:25:59'),
(134, 1, 'Exit animal weight', 'animal_profile', 3, 'animal', '2018-11-28 11:26:00'),
(135, 1, 'Viewed animal history', 'animal_profile', 3, 'animal', '2018-11-28 11:26:03'),
(136, 1, 'Viewed animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 11:26:28'),
(137, 1, 'Export animal profile', 'animal_profile', 3, 'animal', '2018-11-28 11:26:39'),
(138, 1, 'Login successfully', 'user', 1, '', '2018-11-28 11:28:52'),
(139, 1, 'Manage User', '', 0, '', '2018-11-28 11:29:26'),
(140, 1, 'Manage animal', '', 0, '', '2018-11-28 11:29:58'),
(141, 1, 'Edit Animal Date_birth', 'animal_profile', 3, 'animal', '2018-11-28 11:32:04'),
(142, 1, 'Login successfully', 'user', 1, '', '2018-11-28 17:25:37'),
(143, 1, 'Master Datalog', '', 0, '', '2018-11-28 17:25:53'),
(144, 1, 'Exit master datalog', '', 0, '', '2018-11-28 17:26:04'),
(145, 1, 'Manage Location', '', 0, '', '2018-11-28 17:26:19'),
(146, 1, 'Delete location', '', 0, '', '2018-11-28 17:26:28'),
(147, 1, 'Exit manage location', '', 0, '', '2018-11-28 17:26:32'),
(148, 1, 'Master Datalog', '', 0, '', '2018-11-28 17:26:36'),
(149, 1, 'Exit master datalog', '', 0, '', '2018-11-28 17:26:59'),
(150, 1, 'Manage User', '', 0, '', '2018-11-28 17:27:05'),
(151, 1, 'Update user and exit manage user', '', 0, '', '2018-11-28 17:28:09'),
(152, 1, 'Manage Location', '', 0, '', '2018-11-28 17:28:32'),
(153, 1, 'Exit manage location', '', 0, '', '2018-11-28 17:29:33'),
(154, 1, 'Manage User', '', 0, '', '2018-11-28 17:29:35'),
(155, 1, 'Exit manage user', '', 0, '', '2018-11-28 17:29:57'),
(156, 1, 'Manage animal', '', 0, '', '2018-11-28 17:30:12'),
(157, 1, 'Exit manage animal', '', 0, '', '2018-11-28 17:30:24'),
(158, 1, 'Manage Location', '', 0, '', '2018-11-28 17:30:28'),
(159, 1, 'Exit manage location', '', 0, '', '2018-11-28 17:30:34'),
(160, 1, 'Manage animal', '', 0, '', '2018-11-28 17:30:49'),
(161, 1, 'Viewed animal weight', 'animal_profile', 3, 'animal', '2018-11-28 17:31:18'),
(162, 1, 'Add animal weight', 'animal_profile', 3, 'animal', '2018-11-28 17:31:35'),
(163, 1, 'Click on OBS', 'animal_profile', 3, 'animal', '2018-11-28 17:31:35'),
(164, 1, 'Click on OBS', 'animal_profile', 3, 'animal', '2018-11-28 17:31:39'),
(165, 1, 'Exit animal weight', 'animal_profile', 3, 'animal', '2018-11-28 17:31:55'),
(166, 1, 'Viewed animal history', 'animal_profile', 3, 'animal', '2018-11-28 17:31:58'),
(167, 1, 'Exit animal history', 'animal_profile', 3, 'animal', '2018-11-28 17:32:06'),
(168, 1, 'Viewed animal history', 'animal_profile', 3, 'animal', '2018-11-28 17:32:09'),
(169, 1, 'Add animal history', 'animal_profile', 3, 'animal', '2018-11-28 17:32:13'),
(170, 1, 'Exit animal history', 'animal_profile', 3, 'animal', '2018-11-28 17:32:15'),
(171, 1, 'Viewed animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 17:32:18'),
(172, 1, 'Export animal datalog', 'animal_profile', 3, 'animal', '2018-11-28 17:32:34'),
(173, 1, 'Export animal profile', 'animal_profile', 3, 'animal', '2018-11-28 17:32:43'),
(174, 1, 'Export animal profile', 'animal_profile', 3, 'animal', '2018-11-28 17:32:48'),
(175, 1, 'Login successfully', 'user', 1, '', '2018-11-29 11:45:34'),
(176, 1, 'Create New Animal', '', 0, '', '2018-11-29 11:45:38'),
(177, 1, 'Login successfully', 'user', 1, '', '2018-11-29 13:28:07'),
(178, 1, 'Login successfully', 'user', 1, '', '2018-11-30 15:49:04'),
(179, 1, 'Viewed animal history', 'animal_profile', 3, 'animal', '2018-11-30 16:29:47'),
(180, 1, 'Login successfully', 'user', 1, '', '2018-11-30 17:32:56'),
(181, 1, 'Manage Location', '', 0, '', '2018-11-30 17:33:02'),
(182, 1, 'Manage User', '', 0, '', '2018-11-30 17:33:06'),
(183, 1, 'Manage animal', '', 0, '', '2018-11-30 17:35:19'),
(184, 1, 'Login successfully', 'user', 1, '', '2018-12-03 18:29:13'),
(185, 1, 'Login successfully', 'user', 1, '', '2018-12-03 18:29:13'),
(186, 1, 'Login successfully', 'user', 1, '', '2018-12-03 18:30:50'),
(187, 1, 'Login successfully', 'user', 1, '', '2018-12-08 13:19:49'),
(188, 1, 'Manage animal', '', 0, '', '2018-12-08 13:19:58'),
(189, 1, 'Login successfully', 'user', 1, '', '2018-12-17 16:43:43'),
(190, 1, 'Login successfully', 'user', 1, '', '2018-12-17 16:44:00'),
(191, 1, 'Manage animal', '', 0, '', '2018-12-17 16:44:04'),
(192, 1, 'Exit manage animal', '', 0, '', '2018-12-17 16:44:17'),
(193, 1, 'Master Datalog', '', 0, '', '2018-12-17 16:44:20'),
(194, 1, 'Exit master datalog', '', 0, '', '2018-12-17 16:44:23'),
(195, 1, 'Login successfully', 'user', 1, '', '2019-01-18 17:16:46'),
(196, 1, 'Create New Animal', '', 0, '', '2019-01-18 17:16:56'),
(197, 1, 'Manage animal', '', 0, '', '2019-01-18 17:18:40'),
(198, 1, 'Exit manage animal', '', 0, '', '2019-01-18 17:19:12'),
(199, 1, 'Master Datalog', '', 0, '', '2019-01-18 17:19:16'),
(200, 1, 'Exit master datalog', '', 0, '', '2019-01-18 17:19:26'),
(201, 1, 'Manage Location', '', 0, '', '2019-01-18 17:19:29'),
(202, 1, 'Master Datalog', '', 0, '', '2019-01-18 17:19:35'),
(203, 1, 'Create New Animal', '', 0, '', '2019-01-18 17:19:40');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'max_weight', '9.99'),
(2, 'weight_difference', '0.5'),
(3, 'obs_count', '5'),
(4, 'selected_status', '1'),
(5, 'log_count', '3'),
(6, 'block_time', '5');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_code` varchar(255) NOT NULL,
  `location_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `last_login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `log_count` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `modified_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_code`, `location_id`, `name`, `email`, `role`, `last_login_time`, `log_count`, `deleted_at`, `modified_at`, `created_at`) VALUES
(1, '1234567890987', 1, 'admin', 'admin@gmail.com', 'superadmin', '2018-11-23 07:35:56', 1, NULL, '2018-11-23 07:35:56', '2018-10-17 18:30:00'),
(40, '8566841554321', 1, 'Test', '', 'user', '2018-11-28 04:50:19', 3, '2018-11-28 04:50:19', '2018-11-28 04:50:19', '2018-11-27 12:54:08'),
(41, '5532543254354', 1, 'User test', '', 'user', '2018-11-28 04:43:23', 0, '2018-11-28 04:43:23', '2018-11-28 04:43:23', '2018-11-28 04:43:05'),
(42, '5645645654645', 3, 'User test', '', 'user', '2018-11-28 11:58:09', 0, NULL, '2018-11-28 17:28:09', '2018-11-28 04:49:54');

-- --------------------------------------------------------

--
-- Table structure for table `weight_observation`
--

CREATE TABLE `weight_observation` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `animal_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `obs_count` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weight_observation`
--

INSERT INTO `weight_observation` (`id`, `created_at`, `animal_id`, `user_id`, `obs_count`, `status`) VALUES
(1, '2018-11-28 10:57:02', 3, 1, 5, 0),
(2, '2018-11-28 10:57:27', 3, 1, 5, 0),
(3, '2018-11-28 11:25:59', 3, 1, 5, 0),
(4, '2018-11-28 17:31:35', 3, 1, 5, 0),
(5, '2018-11-28 17:31:39', 3, 1, 5, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `animal_history`
--
ALTER TABLE `animal_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animal_profile`
--
ALTER TABLE `animal_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animal_status`
--
ALTER TABLE `animal_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `animal_weight`
--
ALTER TABLE `animal_weight`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_associated_email`
--
ALTER TABLE `location_associated_email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_email`
--
ALTER TABLE `location_email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manage_location`
--
ALTER TABLE `manage_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_datalog`
--
ALTER TABLE `master_datalog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `weight_observation`
--
ALTER TABLE `weight_observation`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `animal_history`
--
ALTER TABLE `animal_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `animal_profile`
--
ALTER TABLE `animal_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `animal_status`
--
ALTER TABLE `animal_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `animal_weight`
--
ALTER TABLE `animal_weight`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `location_associated_email`
--
ALTER TABLE `location_associated_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `location_email`
--
ALTER TABLE `location_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `manage_location`
--
ALTER TABLE `manage_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `master_datalog`
--
ALTER TABLE `master_datalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `weight_observation`
--
ALTER TABLE `weight_observation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
