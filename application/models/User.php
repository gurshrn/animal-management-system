<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



class User extends CI_Model {



    public function __construct() {

        parent::__construct();

    }

	

	public function loginUser($data)

    {

		$this->db->select('*');		

		$this->db->from('user');		

		$this->db->where('user_code', $data['user']);		

		$this->db->where('deleted_at IS NULL', null, true);		

		$result = $this->db->get();		

		$result = $result->row();

		return $result;

    }

/*==================Add User===============*/

    public function addUser($param)
	{
		foreach($param['group'] as $val)
		{
			unset($val['delete_id']);
			$val['role'] = 'user';
			$val['name'] = ucfirst($val['name']);
			if($val['id'] == '')
			{
				$val['created_at'] = date('Y-m-d H:i:s');
				$val['modified_at'] = date('Y-m-d H:i:s');
				$this->db->insert('user',$val);
				$lastId = $this->db->insert_id();
				$successMsg = 'User added successfully.';
				$errorsMsg = 'User not added successfully.';
				$type = 'Add';
			}
			else
			{
				$val['modified_at'] = date('Y-m-d H:i:s');
				$this->db->where('id',$val['id']);
				$this->db->update('user',$val);
				$lastId = $val['id'];
				$successMsg = 'User updated successfully.';
				$errorsMsg = 'User not updated successfully.';
				$type = 'Update';
			}
			$db_error = $this->db->error();
			if ($db_error['code'] == 0) 
			{
				$result['success'] = true;
				$result['id'] = $lastId;
				$result['type'] = $type;
				$result['success_msg'] = $successMsg;
			}
			else
			{
				$result['error'] = false;
				$result['error_msg'] = $errorsMsg;
			}
		}
		return $result;
	}

/*==================Show User List===============*/

	public function getUserList()
	{
		$this->db->select('*');		
		$this->db->from('user');		
		$this->db->where('id !=', 1);		
		$this->db->where('deleted_at IS NULL', null, true);	
		$this->db->order_by('id','DESC');		
		$result = $this->db->get();	
		$result = $result->result();
		return $result;
	}

/*==================Get User Detail according to usercode===============*/

	public function getUserDetail($usercode)
	{
		$this->db->select('*');		
		$this->db->from('user');
		$this->db->where('user_code',$usercode);
		$this->db->where('deleted_at IS NULL', null, true);
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}

/*==================Get Animal Detail according to animalcode===============*/

	public function checkAnimalCode($param)
	{
			$this->db->select('*');		
			$this->db->from('animal_profile');
			$this->db->where('animal_code',$param['animal_code']);
			$this->db->where('deleted_at IS NULL', null, true);
			$result = $this->db->get();		
			$result = $result->row();
			return $result;
	}

/*==================Get Location List if emails of location exist===============*/

	public function getLocationListForAnimal()
	{
		$this->db->select('manage_location.*');		
		$this->db->from('manage_location');		
		$this->db->join('location_associated_email','location_associated_email.location_id = manage_location.id','inner');		
		$this->db->where('manage_location.deleted_at IS NULL', null, true);
		$this->db->group_by('manage_location.id');
		$this->db->order_by('manage_location.location_name','ASC');
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}

/*==================Get Location List===============*/

	public function getLocationList()
	{
		$this->db->select('*');		
		$this->db->from('manage_location');		
		$this->db->where('deleted_at IS NULL', null, true);
		$this->db->order_by('location_name','DESC');
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}

/*==================Add Location================*/

	public function addLocation($param)
	{
		$userId = $this->session->userdata('userId');
		foreach($param as $val)
		{
			if($val['name'] != '')
			{

				if($val['id'] == '')
				{

					$this->db->set('created_at',date('Y-m-d H:i:s'));
					$this->db->set('modified_at',date('Y-m-d H:i:s'));
					$this->db->set('location_name',ucfirst($val['name']));
					$this->db->set('user_id',$userId);
					$this->db->insert('manage_location');
					$insertId = $this->db->insert_id();
					$db_error = $this->db->error();
					if ($db_error['code'] == 0) 
					{
						

						$result['success'] = true;
						$result['lastId'] = $insertId;
					}
					else
					{

						$result['error'] = false;
					}
				}
				else
				{
					$this->db->set('modified_at',date('Y-m-d H:i:s'));
					$this->db->set('location_name',ucfirst($val['name']));
					$this->db->set('user_id',$userId);
					$this->db->where('id',$val['id']);
					$this->db->update('manage_location');
					$db_error = $this->db->error();
					if ($db_error['code'] == 0) 
					{
						
						$result['success'] = true;
					}
					else
					{
						$result['error'] = false;
					}
				}
			}
		}
		return $result;
	}

/*==================Get Location Email List================*/	

	public function getLocationEmailList($locationId,$columns='*')
	{
		$this->db->select($columns);		
		$this->db->from('location_associated_email');		
		$this->db->where('deleted_at IS NULL', null, true);
		$this->db->where('location_id',$locationId);
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}

/*==================Add Location Emails================*/

	public function addLocationEmails($param)
	{
		$userId = $this->session->userdata('userId');
		$line_data = explode("\n", $param["email"]);
		$invalidEmail = '';
		$this->db->select('email');		
		$this->db->from('location_associated_email');		
		$this->db->where('deleted_at IS NULL', null, true);
		$this->db->where('location_id', $param['location_id']);
		$result = $this->db->get();
		$result = $result->result_array();
		foreach($result as $val)
		{
			$results[] = $val['email'];
		}

		$email = array_values($line_data);

		if(!empty($result))
		{
			$this->db->set('deleted_at',date('Y-m-d H:i:s'));
			$this->db->where('location_id',$param['location_id']);
			$this->db->where_not_in('email',$line_data);
			$this->db->update('location_associated_email');
			$email = array_values(array_diff($line_data, $results));
			$db_error = $this->db->error();
			if ($db_error['code'] == 0) 
			{
				$response['success'] = true;
			}
			else
			{
				$response['error'] = false;
			}
		}

		$email = array_filter($email, function($v){ return trim($v); });
		if(!empty($email))
		{
			$a = array_fill(0,count($email),'email');

			$new_array = array(); 
			$location_id = $param['location_id'];
			array_walk($email,
		        function ($item, $key) use (&$new_array,$location_id,$userId) {
		            $new_array[]=['email' => $item,'location_id' => $location_id,'user_id' => $userId,'created_at'=>date('Y-m-d H:i:s'),'modified_at'=>date('Y-m-d H:i:s')];
		        }
		    );

		    $this->db->insert_batch('location_associated_email',$new_array);
		    $db_error = $this->db->error();
			if ($db_error['code'] == 0) 
			{
				$response['success'] = true;
			}
			else
			{
				$response['error'] = false;
			}
			
		}
		return $response;
	}
	
/*==============Check user ip address in login attempt================*/
	
	public function checkIp($ip)
	{
		$this->db->select('*');		
		$this->db->from('login_attempts');
		$this->db->where('ip_address',$ip);
		$result = $this->db->get();		
		$result = $result->row();
		return $result;
	}

/*==============Add User Login Attempt================*/

	public function addUserLoginAttempt($param)
	{
		$this->db->insert('login_attempts',$param);
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$response['success'] = true;
		}
		else
		{
			$response['error'] = false;
		}
	}

/*==============Update User Login Attempt================*/

	public function updateUserLoginAttempt($lastLoginTime,$logCount,$ipAdd)
	{
		if($lastLoginTime != '')
		{
			$this->db->set('last_login_time',$lastLoginTime);
		}
		$this->db->set('modified_at',date('Y-m-d H:i:s'));
		$this->db->set('log_count',$logCount);
		$this->db->where('ip_address',$ipAdd);
		$this->db->update('login_attempts');
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$response['success'] = true;
		}
		else
		{
			$response['error'] = false;
		}
	}

/*==============Update User Log Count in User table================*/

	public function updateLogCount($count,$userId)
	{
		$this->db->select('log_count');		
		$this->db->from('user');		
		$this->db->where('id', $userId);
		$result = $this->db->get();		
		$result = $result->row();
		if($result->log_count <= 3)
		{
			$this->db->set('last_login_time',date('Y-m-d H:i:s'));
		}
		$this->db->set('modified_at',date('Y-m-d H:i:s'));
		$this->db->set('log_count',$count);
		$this->db->where('id',$userId);
		$this->db->update('user');
	}

/*=================Common Delete=================*/

	public function commonDelete($param,$tableName)
	{
		if($tableName == 'animal_profile')
		{

			$delete_id = $param['is_delete'];
		}
		else
		{
			foreach($param['group'] as $val)
			{
				if(isset($val['is_delete']) && !empty($val['is_delete']))
				{
					$delete_id[] = $val['delete_id'];
				}
			}
		}
		if(isset($delete_id) && !empty($delete_id))
		{
			$deletedId = implode(",",$delete_id);
			$this->db->set('deleted_at',date('Y-m-d H:i:s'));
			$this->db->where_in('id',$delete_id);
			$this->db->update($tableName);
			$db_error = $this->db->error();
			if ($db_error['code'] == 0) 
			{
				$result['success'] = true;
				if($tableName == 'manage_location')
				{
					$this->db->set('deleted_at',date('Y-m-d H:i:s'));
					$this->db->where_in('location_id',$delete_id);
					$this->db->where('deleted_at IS NULL', null, true);
					$this->db->update('location_associated_email');
				}
			}
			else
			{
				$result['error'] = false;
			}
			return $result;
		}
	}

/*=================Master Datalog List=================*/

	public function getMasterDatalog()
	{
		$this->db->select('*');		
		$this->db->from('master_datalog');
		$this->db->where('datalog-type !=','animal');
		$this->db->order_by('id','DESC');		
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}
	public function checkName($param)
	{
		$this->db->select('*');		
		$this->db->from('manage_location');
		$this->db->where('location_name',$param['locationName']);
		$this->db->where('deleted_at IS NULL', null, true);
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}
	

}