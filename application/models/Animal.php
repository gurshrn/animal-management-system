<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class Animal extends CI_Model {

	public function __construct() {

        parent::__construct();

    }

	public function getStatus()
	{
		$this->db->select('*');		
		$this->db->from('animal_status');		
		$this->db->where('deleted_at IS NULL', null, true);
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}

/*=================Add animal=================*/

	public function addAnimal($param,$imageName)
	{
		$this->db->select('*');
		$this->db->from('animal_profile');
		$this->db->where('animal_code',$param['animal_code']);
		$this->db->where('animal_profile.deleted_at IS NULL', null, true);
		$result = $this->db->get();		
		$result = $result->result();
		if(count($result) == 0)
		{
			$userId = $this->session->userdata('userId');
			$data = array(
				'name'=>$param['name'],
				'status_id'=>$param['status_id'],
				'location_id'=>$param['location_id'],
				'date_birth'=>$param['date_birth'],
				'date_arrived'=>$param['date_arrived'],
				'chip_number'=>$param['chip_number'],
				'pod_number'=>$param['pod_number'],
				'neuter_date'=>$param['neuter_date'],
				'first_injection'=>$param['first_injection'],
				'second_injection'=>$param['second_injection'],
				'animal_code'=>$param['animal_code'],
				'image'=>$imageName,
				'ageYear'=>$param['year'],
				'ageMonth'=>$param['month'],
				'ageDay'=>$param['day'],
				'modified_at'=>date('Y-m-d H:i:s'),
			);
			if($param['id'] == '')
			{
				$data['user_id'] = $userId;
				$data['created_at'] = date('Y-m-d H:i:s');
				$this->db->insert('animal_profile',$data);
				$successMsg = 'Animal added successfully';
				$errorMsg = 'Animal not added successfully';
				$db_error = $this->db->error();
				if ($db_error['code'] == 0) 
				{

					$result['success'] = true;
					$result['success_message'] = $successMsg;
				}
				else
				{
					$result['error'] = false;
					$result['error_message'] = $errorMsg;
				}
			}
		}
		else
		{
			$result['error'] = false;
			$result['error_message'] = 'Animal code already exist';
		}
		return $result;
	}

/*=================Get Animal List===============*/

	public function getAnimalList($id = NULL)
	{
		$this->db->select('animal_profile.*,(select location_name from manage_location where id= animal_profile.location_id and deleted_at is null) as location_name');		
		$this->db->from('animal_profile');
		$this->db->where('animal_profile.deleted_at IS NULL', null, true);
		if($id != '')
		{
			$this->db->where('animal_profile.id',$id);
		}
		$this->db->order_by('animal_profile.name','ASC');		
		$result = $this->db->get();
		$result = $result->result();
		return $result;
	}

	public function getAnimalListBySort($param)
	{
		$this->db->select('animal_profile.*,(select location_name from manage_location where id= animal_profile.location_id and deleted_at is null) as location_name');		
		$this->db->from('animal_profile');
		$this->db->where('animal_profile.deleted_at IS NULL', null, true);
		$this->db->order_by($param['sortBy'],$param['sortType']);		
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}

/*=================Get Animal Weight List===============*/

	public function getWeightList($animalId)
	{
		$this->db->select('*');		
		$this->db->from('animal_weight');
		$this->db->where('deleted_at IS NULL', null, true);
		$this->db->where('animal_id',$animalId);
		$this->db->order_by('id','DESC');		
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}

/*=================Get Animal Previous Weight List===============*/

	public function getPreviousWeight($animalId)
	{
		$this->db->select('weight');		
		$this->db->from('animal_weight');
		$this->db->where('deleted_at IS NULL', null, true);
		$this->db->where('animal_id',$animalId);
		$this->db->order_by('id','DESC');
		$result = $this->db->get();		
		$result = $result->row();
		return $result;
	}

/*=================Add  Animal Weight===============*/

	public function addAnimalWeight($param)
	{
		$userId = $this->session->userdata('userId');
		$data = array(
			'animal_id'=>$param['animalId'],
			'weight'=>$param['animalWeight'],
			'created_at'=>$param['datetime'],
			'modified_at'=>date('Y-m-d H:i:s'),
			'user_id'=>$userId,
		);
		$this->db->insert('animal_weight',$data);
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{

			$result['success'] = true;
		}
		else
		{
			$result['error'] = false;
		}
		return $result;
	}

/*=================Add  Weight Observation===============*/

	public function addObs($param)
	{
		$obsCount = getSettings('obs_count');
		$userId = $this->session->userdata('userId');
		$data = array(
			'user_id'=>$userId,
			'obs_count'=>$obsCount->value,
			'animal_id'=>$param,
			'status'=>0,
			'created_at'=>date('Y-m-d H:i:s'),
		);
		$this->db->select('*');
		$this->db->from('weight_observation');
		$this->db->where('status',0);
		$this->db->where('animal_id',$param);
		$query = $this->db->get();	
		$query = $query->row();
		if(count($query) == 0)
		{
			$this->db->insert('weight_observation',$data);
			
		}
		else
		{
			if($query->status == 1)
			{
				$this->db->where('animal_id',$param);
				$this->db->update('weight_observation',$data);
			}
			
		}	
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{

			$result['success'] = true;
		}
		else
		{
			$result['error'] = false;
		}
		return $result;
	}

/*=================Add  Animal History===============*/

	public function addAnimalHistory($param)
	{
		$userId = $this->session->userdata('userId');
		$data = array(
			'animal_id'=>$param['animalId'],
			'comment'=>$param['comment'],
			'date'=>$param['date'],
			'time'=>$param['time'],
			'user_id'=>$userId,
			'created_at'=>date('Y-m-d H:i:s'),
			'modified_at'=>date('Y-m-d H:i:s'),
		);
		$this->db->insert('animal_history',$data);
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$result['success'] = true;
			$result['user_id'] = $userId;
		}
		else
		{
			$result['error'] = false;
		}
		return $result;
	}

/*=================Get Animal History List===============*/

	public function getAnimalHistory($animalId)
	{
		$this->db->select('*');		
		$this->db->from('animal_history');
		$this->db->where('deleted_at IS NULL', null, true);
		$this->db->where('animal_id',$animalId);
		$this->db->order_by('id','DESC');
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}

/*===========Animal Datalog list=============*/

	public function getAnimalDatalog($id)
	{
		$this->db->select('*');		
		$this->db->from('master_datalog');
		$this->db->where('datalog-type','animal');
		$this->db->where('table_id',$id);
		$this->db->order_by('id','DESC');
		$result = $this->db->get();		
		$result = $result->result();
		return $result;
	}

/*=============Update Profile image==============*/

	public function updateProfilePic($param)
	{
		$userId = $this->session->userdata('userId');
		$this->db->set('image',$param['imageName']);
		$this->db->set('modified_at',date('Y-m-d H:i:s'));
		$this->db->where('id',$param['editId']);
		$this->db->update('animal_profile');
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$data = array(
				'user_id'=>$userId,
				'action'=>'Edit animal profile image',
				'table_name' => 'animal_profile',
				'table_id' => $param['editId'],
				'datalog-type'=>'animal',
				'created_at'=>date('Y-m-d H:i:s'),
			);
			$this->db->insert('master_datalog',$data);
			$result['success'] = true;
		}
		else
		{
			$result['error'] = false;
		}
		return $result;
	}

/*=============Update Animal Information==============*/

	public function updateAnimal($param)
	{
		$userId = $this->session->userdata('userId');

		if($param['inputName'] == 'date_birth')
		{
			$from = new DateTime(date('Y-m-d',strtotime($param['inputVal'])));
			$to   = new DateTime('today');
			$year =  $from->diff($to)->y;
			$month =  $from->diff($to)->m;
			$day =  $from->diff($to)->d;
			$this->db->set('ageYear',$year.' Y');
			$this->db->set('ageMonth',$month.' M');
			$this->db->set('ageDay',$day.' D');
		}
		$this->db->set('modified_at',date('Y-m-d H:i:s'));
		$this->db->set($param['inputName'],$param['inputVal']);
		$this->db->where('id',$param['editId']);
		$this->db->update('animal_profile');
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{
			$data = array(
				'user_id'=>$userId,
				'action'=>'Edit Animal'.' '.ucfirst($param['inputName']),
				'table_name' => 'animal_profile',
				'table_id' => $param['editId'],
				'datalog-type'=>'animal',
				'created_at'=>date('Y-m-d H:i:s')
			);
			$this->db->insert('master_datalog',$data);
			$result['success'] = true;
		}
		else
		{
			$result['error'] = false;
		}
		return $result;
	}

/*=================Add Master Datalog===============*/

	public function masterDatalog($param)
	{
		$userId = $this->session->userdata('userId');
		if(isset($param['user_id']) && $userId == '')
		{
			$userId = $param['user_id'];
		}
		else
		{
			$userId = $userId;
		}
		$data = array(
			'user_id'=>$userId,
			'action'=>$param['actionPerform'],
			'created_at'=>date('Y-m-d H:i:s'),
		);
		if(isset($param['tablename']))
		{
			if($param['tablename'] == 'animal_profile')
			{
				$data['datalog-type'] = 'animal';
			}
			$data['table_name'] = $param['tablename'];
		}
		if(isset($param['tableId']))
		{
			$data['table_id'] = $param['tableId'];
		}
		$this->db->insert('master_datalog',$data);
		$db_error = $this->db->error();
		if ($db_error['code'] == 0) 
		{

			$result['success'] = true;
		}
		else
		{
			$result['error'] = false;
		}
		return $result;

	}
}