<?php
	if (!defined('BASEPATH'))
	exit('No direct script access allowed');

	class Cron extends CI_Model {

	public function __construct() {

        parent::__construct();

    }

	public function getAnimalList($type)
	{
		$this->db->select('*');
		$this->db->from('animal_profile');
		if($type == 'secondInj')
		{
			$this->db->where('second_injection'," ");
			//$this->db->where('DATE(created_at) = (DATE_SUB(CURDATE(), INTERVAL 14 MINUTES ) )');
			$this->db->where('created_at >= (DATE_SUB(CURDATE(), INTERVAL 14 MINUTE ) )');
		}
		if($type == 'profile')
		{
			//$this->db->where('DATE(created_at) = (DATE_SUB(CURDATE(), INTERVAL 89 DAY ) )');
			$this->db->where('created_at >= (DATE_SUB(CURDATE(), INTERVAL 10 MINUTE ) )');
		}
		$result = $this->db->get();	
		$result = $result->result();
		return $result;
	}
	public function getWeightObsList()
	{
		$this->db->select('*');
		$this->db->from('weight_observation');
		$this->db->where('status',0);
		$result = $this->db->get();	
		$result = $result->result();
		return $result;
	}
	public function getAnimalWeighttList($animalId)
	{
		$currentDate = date('Y-m-d');
		$this->db->select('*');
		$this->db->from('animal_weight');
		$this->db->where('animal_id',$animalId);
		$this->db->where('DATE(modified_at)',$currentDate);
		$result = $this->db->get();	
		$result = $result->result();
		return $result;
	}
	public function getLocationId($animalId)
	{
		$this->db->select('location_id,animal_code');
		$this->db->from('animal_profile');
		$this->db->where('id',$animalId);
		$result = $this->db->get();	
		$result = $result->row();
		return $result;
	}
	public function updateCount($count,$animalId)
	{
		if($count == 0)
		{
			$this->db->set('status',1);
		}
		$this->db->set('obs_count',$count);
		$this->db->where('animal_id',$animalId);
		$this->db->update('weight_observation');
	}
}