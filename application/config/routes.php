<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'UserController/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*========User controller routes========*/
$route['user-login'] = 'UserController/userLogin';
$route['logout'] = 'UserController/logout';
$route['dashboard'] = 'UserController/dashboard';
$route['add-user'] = 'UserController/userList';
$route['get-user-list'] = 'UserController/getUserList';
$route['manage-users'] = 'UserController/userList';
$route['delete-user'] = 'UserController/deleteUser';
$route['manage-location'] = 'UserController/manageLocation';
$route['get-location-list'] = 'UserController/locationList';
$route['location-email-list'] = 'UserController/emailList';
$route['manage-location-email'] = 'UserController/manageLocationEmail';
$route['delete-manage-location'] = 'UserController/deleteManageLocation';
$route['master-datalog'] = 'UserController/getMasterLog';
$route['check-user-code'] = 'UserController/checkUserCode';
$route['check-location-name'] = 'UserController/checkLocationName';

/*========Animal controller routes========*/
$route['animal-list-sorting'] = 'AnimalController/animalListSorting';
$route['add-animal'] = 'AnimalController/index';
$route['animal-profile'] = 'AnimalController/index';
$route['manage-animal'] = 'AnimalController/manageAnimal';
$route['delete-manage-animal'] = 'AnimalController/deleteManageAnimal';
$route['add-animal-weight'] = 'AnimalController/manageWeight';
$route['add-weight-obs'] = 'AnimalController/addWeightObs';
$route['add-animal-history'] = 'AnimalController/manageAnimalHistory';
$route['manage-animal-datalogs'] = 'AnimalController/manageDataLogs';
$route['update-animal-profile'] = 'AnimalController/updateAnimalProfile';
$route['export-animal-profile'] = 'AnimalController/exportAnimalProfile';
$route['export-master-datalog'] = 'AnimalController/exportMasterDatalog';
$route['animal-datalog/(:any)'] = 'AnimalController/manageAnimalDataLog/$1';
$route['animal-history/(:any)'] = 'AnimalController/manageAnimalHistory/$1';
$route['weight/(:any)'] = 'AnimalController/manageWeight/$1';
$route['edit-animal/(:any)'] = 'AnimalController/index/$1';
$route['export-weight/(:any)'] = 'AnimalController/exportAnimalWeight/$1';
$route['export-animal-history/(:any)'] = 'AnimalController/exportAnimalHistory/$1';
$route['export-animaldatalog/(:any)'] = 'AnimalController/exportAnimalDatalog/$1';

/*========Cron controller routes========*/
$route['second-injection-cron'] = 'CronController/secondInjectionCron';
$route['profile-image-cron'] = 'CronController/profileImageCron';
$route['weight-obs-cron'] = 'CronController/weightObsCron';
$route['common-cron'] = 'CronController/cron';

