<table class="table" align="center">
    <thead>
        <tr>
            <th colspan="5">Details</th>
        </tr>
    </thead>
    <tbody>
         <?php 
            if(isset($animalHistory) && !empty($animalHistory))
            {
                foreach($animalHistory as $val)
                {
                    $userdata = getUserData($val->user_id);
                    
                    if($userdata->role == 'superadmin')
                    {
                        $userid = 'Master User';
                    }
                    else
                    {
                        $userid = 'USER'.' '.$userdata->user_code;
                    }
                    echo '<tr>';
                    echo '<td>'.date('d/m/Y',strtotime($val->created_at)).'</td>';
                    echo '<td>'.date('H:i',strtotime($val->created_at)).'</td>';
                    echo '<td>'.$userid.'</td>';
                    echo '<td>-</td>';
                    echo '<td>'.$val->comment.'</td>';
                    echo '</tr>';
                }
            }
            else
            {
                echo '<tr><td colspan="5">No record found...</td></tr>';
            }

        ?>
        

    </tbody>
</table>