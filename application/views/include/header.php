<!doctype html>

<html lang="en">



    <head>

        <meta charset="utf-8">

        <title>Animal</title>

        <link rel="icon" href="images/favicon.png" type="image/x-icon">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">

        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <!-- Favicon -->

        <link rel="shortcut icon" href="favicon.png" sizes="32x32" type="image/x-icon">



        <!-- CSS -->


        <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/bootstrap.min.css">

        <!--link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/bootstrap-datepicker.min.css"-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">



        <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/font-awesome.min.css">

		<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

        <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/jquery.mCustomScrollbar.min.css">
        <link rel="stylesheet" href="<?php echo site_url(); ?>assets/css/style.css">



		<script>

			var site_url = '<?php echo site_url(); ?>';

		</script>

    </head>
