<?php 

	$username = $this->session->userdata('name');

	$userid = $this->session->userdata('userId');

	$userData = getUserData($userid);

?>

<body>

	<header>

		<div class="container">

			<nav class="navbar navbar-expand-lg">

				<a class="navbar-brand" href="javascript:void(0)"><img src="<?php echo site_url(); ?>assets/images/logo.png" alt="logo"></a>

				<div class="account-wrap">

					<div class="account-item clearfix js-item-menu">

						<div class="content"> <a class="js-acc-btn" href="javascript:void(0)"><?php echo ucfirst($username);?></a> </div>

						<div class="account-dropdown js-dropdown">

							<div class="info clearfix">

								<div class="content">

									<h5 class="name">

										<a href="#"><?php echo ucfirst($username);?></a>

									</h5> <span class="email"><?php echo $userData->email;?></span>

								</div>

							</div>

							<div class="account-dropdown__footer">

								<a href="<?php echo site_url('logout');?>"> <i class="fa fa-power-off" aria-hidden="true"></i>Logout</a>

							</div>

						</div>

					</div>

				</div>

			</nav>

		</div>

	</header>
	<div class="preloader" style="display:none;">
		<div class="loader"></div>
	</div>