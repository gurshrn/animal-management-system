<table class="table" align="center">
    <thead>
        <tr>
            <th colspan="5">Details</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if(isset($animaldatalog) && !empty($animaldatalog))
            {
                foreach($animaldatalog as $val)
                {
                    $userdata = getUserData($val->user_id);
                    if($val->table_id != 0)
                    {
                        $animalData = getAnimalData($val->table_id);
                        $animalCode = $animalData->animal_code;
                    }
                    else
                    {
                        $animalCode = '';
                    }
                    
                    if($userdata->role == 'superadmin')
                    {
                        $userid = 'Master User';
                    }
                    else
                    {
                        $userid = 'USER'.' '.$userdata->user_code;
                    }
                    echo '<tr>';
                    echo '<td>'.date('d/m/Y',strtotime($val->created_at)).'</td>';
                    echo '<td>'.date('H:i',strtotime($val->created_at)).'</td>';
                    echo '<td>'.$userid.'</td>';
                    echo '<td>-</td>';
                    echo '<td>'.ucfirst($val->action).' '.$animalCode.'</td>';
                }
            }
            else
            {
                echo '<tr><td>No record found.</td></tr>';
            }
        ?>
    </tbody>
</table>