<?php
    $this->load->view('include/header');
    $this->load->view('include/navbar');
    $userRole = $this->session->userdata('role');
    $animalId = $this->uri->segment(2);
    $maxWeight = getSettings('max_weight');
    $animalObs = getObs($animalId);
   

?>
        <section class="animal-profile">
            <div class="container">
                <h2>Weight</h2>
                <div class="profile_content weight">
                    <div class="row">
                        <div class="col-sm-3 col-12">
                            <div class="weight_size">
                                <h4>Enter Weight</h4>
                                <div class="point">
                                    <input type="hidden" class="animal-id" value="<?php echo $animalId; ?>">
                                    <input type="hidden" class="max-weight" value="<?php echo $maxWeight->value;?>">
                                    <input type="text" class="form_control validNumber animal-weight" maxlength="4">
                                </div>
                                <span>KG</span>
                            </div>
                        </div>
                        <div class="col-sm-9 col-12">
                            <div class="datalog-table">
								   <div class="animal- mCustomScrollbar">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="3">Weight record</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            if(isset($weightList) && !empty($weightList))
                                            {
                                                foreach($weightList as $val)
                                                {
                                                    echo '<tr>';
                                                    echo '<td>'.$val->created_at.'</td>';
                                                    echo '<td>-</td>';
                                                    echo '<td>'.number_format($val->weight,2).'</td>';
                                                    echo '</tr>';
                                                }
                                            }
                                            else
                                            {
                                                echo '<tr><td>No record found...</td></tr>';
                                            }

                                        ?>
                                    </tbody>
								</table>
								</div>
                                <div class="button_submit">
                                    <input type="hidden" value="animal_profile" class="tablename">
                                    <input type="hidden" value="<?php echo $animalId;?>" class="tableid">

                                    <?php 
                                        if($userRole == 'superadmin')
                                        {
                                            if(count($animalObs) > 0)
                                            {
                                                $disabled = 'disabled';
                                            }
                                            else
                                            {
                                                $disabled = '';
                                            }
                                            echo '<button class="export obs action-btn" data-attr="Click on OBS" type="button" '.$disabled.'>OBS</button>';
                                        }


                                    ?>
                                    <a href="<?php echo site_url().'export-weight/'.$animalId;?>"><button class="export action-btn" type="button" data-attr="Export animal weight">Export</button></a>

                                    <button class="exit action-btn" data-target="<?php echo 'edit-animal/'.$animalId;?>" data-attr="Exit animal weight" type="button">Exit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php $this->load->view('include/footer');?>

    </body>
</html>
