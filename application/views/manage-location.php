<?php

	$this->load->view('include/header');

	$this->load->view('include/navbar');



?>

    <section class="animal-profile">

        <div class="container">

            <h2>Manage Location</h2>



                <div class="Manage-loction">

                    <form id="location-manage" action="<?php echo site_url('manage-location');?>" method="POST" autocomplete="off">


                        <input type="hidden" class="table-name" value="manage_location">

                        <input type="hidden" class="get-list" value="get-location-list">

                        <input type="hidden" class="delete-url" value="delete-manage-location">



                    <div class="row">



                        <div class="col-sm-3 col-12 repeater">



                            <div class="location_tabing" >

                                <h4 class="table-head">Location<input data-repeater-create type="button" value="Add"/></h4>

                                <ul class="nav nav-tabs" data-repeater-list="group">

                                    <li class="active location-tab" data-repeater-item>
										<div class="checkbox">
											<label>
                                                <input type="hidden" class="delete-id" name="delete_id">
												<input type="checkbox" id="deleteCheckbox" name="is_delete" data-attr="manage-location"><span class="new-checkbox"></span>
                                                <input type="hidden" name="id" class="location-id" value="">
											<input type="text" name="name" class="form-control location-name alphabets" required="" maxlength="15">
											</label>
										</div>
                                        <!-- <input type="hidden" class="delete-id" name="delete_id">

                                        <input type="checkbox" id="deleteCheckbox" name="is_delete" data-attr="manage-location">

                                        <input type="hidden" name="id" class="location-id" value="">


                                        <input type="text" name="name" class="form-control location-name alphabets" required="" maxlength="15"> -->

                                    </li>

                                </ul>

                            </div>

                        </div>

                        <div class="col-sm-9 col-12">

                            <div class="location-table">

                                <h4 class="table-head">Associate Emails</h4>

                                <div class="tab-content email-content">

                                    <input type="hidden" name="location_id" class="locationId">
                                    <input type="hidden" name="type" class="add-email">

                                    <textarea name="email" class="location-email" rows="10" cols="50" disabled>

                                    </textarea>

                                </div>

                                <div class="button_submit">

                                    <button class="export delete" type="button">Delete</button>

                                    <a href="<?php echo site_url('dashboard');?>"><button class="exit action-btn add-location" data-attr="Exit manage location" type="button">Exit</button>

                                </div>

                            </div>

                        </div>



                    </div>

                    </form>

                </div>



        </div>

    </section>



<?php $this->load->view('include/footer');?>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="delete-modal">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete location?</h4>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-primary yes-delete-user" id="modal-btn-si">Yes</button>

        <button type="button" class="btn btn-warning" data-dismiss="modal" id="modal-btn-no">No</button>

      </div>

    </div>

  </div>

</div>


    </body>



</html>
