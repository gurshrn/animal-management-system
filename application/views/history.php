<?php
    $this->load->view('include/header');
    $this->load->view('include/navbar');
    $animalId = $this->uri->segment(2);
?>
        <section class="animal-profile comment">
            <div class="container">
                <h2>Comment</h2>
                <div class="profile_content">
                    <div class="row">
                        <div class="col-12">
                            <div class="comment">
                                <input type="hidden" class="animal-id" value="<?php echo $animalId; ?>">
                                <textarea class="alphabets animal-history" cols="140" rows="7" maxlength="250"></textarea>
                            </div>
                            <div class="datalog-table">
								<h2>History</h2>
								   <div class="animal- mCustomScrollbar">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="5">Details</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                            if(isset($animalHistory) && !empty($animalHistory))
                                            {
                                                foreach($animalHistory as $val)
                                                {
                                                    $userdata = getUserData($val->user_id);
                                                    if($userdata->role == 'superadmin')
                                                    {
                                                        $userid = 'Master User';
                                                    }
                                                    else
                                                    {
                                                        $userid = 'USER'.' '.$userdata->user_code;
                                                    }
                                                    echo '<tr>';
                                                    echo '<td>'.$val->date.'</td>';
                                                    echo '<td>'.$val->time.'</td>';
                                                    echo '<td>'.$userid.'</td>';
                                                    echo '<td>-</td>';
                                                    echo '<td>'.$val->comment.'</td>';
                                                    echo '</tr>';
                                                }
                                            }
                                            else
                                            {
                                                echo '<tr><td colspan="5">No record found...</td></tr>';
                                            }

                                        ?>


                                    </tbody>
								</table>
								</div>
                                <div class="button_submit">
                                    <input type="hidden" value="animal_profile" class="tablename">
                                    <input type="hidden" value="<?php echo $animalId;?>" class="tableid">

                                    <a href="<?php echo site_url().'export-animal-history/'.$animalId;?>"><button class="export action-btn" data-attr="Export animal history" type="button">Export</button></a>

                                    <a href="<?php echo site_url().'edit-animal/'.$animalId;?>"><button class="exit action-btn" data-attr="Exit animal history" type="button">Exit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<?php $this->load->view('include/footer');?>

    </body>

</html>
