<?php 
    $editAnimals = $editAnimal[0];
?>

<section class="animal-profile" style="padding: 70px 0;">
    <div class="container">
        <h2 style="font-size: 44px;color: #515151;font-family: 'ProximaNova-Bold';text-transform: uppercase;text-align: center;margin-bottom: 25px;">Animal
            Profile</h2>
                <div class="profile_content">
                    <div class="row">
                        <div class="col-sm-3 col-12 profile_wdth">
                            <div class="pic" style="position:relative">
                                <div class="circle" style="overflow: hidden; width: 250px;height: 235px; position: relative;top: 0;">
                                    <img class="profile-pic" style="width:100%; height:100%;" src="<?php echo site_url('assets/upload/'.$editAnimals->image);?>"
                                        alt="Profile">
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-sm-9 col-12 content_wdth">
                            <div class="animal_content" style="background: #fbfbfb; padding: 35px 25px 45px;">
                                <form>
                                    <div class="row">
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group" style="margin-bottom: 25px;">
                                                <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">Name</label>
                                                <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                    class="form-control firstCap" type="text" name="name" placeholder="Fluffy" value="<?php echo $editAnimals->name;?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group" style="margin-bottom: 25px;">
                                                <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">Location</label>
                                                <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                    class="form-control" type="text" name="location" placeholder="LEEDS">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group" style="margin-bottom: 25px;">
                                                <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">Status</label>
                                                <select style="    background: #ed1b24 url(images/dropdown.png) no-repeat 95% 50%;
                                                color: #fff; height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width:
                                                100%;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                    class="form-control">
                                                    <option value="304" selected="">ISO</option>
                                                    <option value="319">ISO #2</option>
                                                    <option value="320">ISO #3</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <div class="row">
                                                <div class="col-5">
                                                    <div class="form-group" style="margin-bottom: 25px;">
                                                        <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">Date
                                                            of Birth</label>
                                                        <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                            class="form-control" type="text" name="birth" value="<?php echo $editAnimals->date_birth;?>">
                                                    </div>
                                                </div>
                                                <div class="col-7 year_wdth">
                                                    <div class="form-group" style="margin-bottom: 25px;">
                                                        <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">Age</label>
                                                        <div class="year" style="width: 43%;display: inline-block;vertical-align: middle;">
                                                            <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                                class="form-control" type="text" name="year"
                                                                placeholder="1 Year">
                                                        </div>
                                                        <div class="month" style="width: 44%;display: inline-block;vertical-align: middle;margin-left: 20px;">
                                                            <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                                class="form-control" type="text" name="month"
                                                                placeholder="03 Month">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-12">
                                            <div class="row">
                                                <div class="col-sm-4 col-12">
                                                    <div class="form-group" style="margin-bottom: 25px;">
                                                        <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">Date
                                                            Arrived</label>
                                                        <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                            class="form-control" type="text" name="date" value="<?php echo $editAnimals->date_arrived;?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-12">
                                                    <div class="form-group" style="margin-bottom: 25px;">
                                                        <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">Chip
                                                            Number</label>
                                                        <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                            class="form-control" type="text" name="chip" value="<?php echo $editAnimals->chip_number;?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-12">
                                                    <div class="form-group" style="margin-bottom: 25px;">
                                                        <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">POD
                                                            Number</label>
                                                        <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                            class="form-control" type="text" name="pod" value="<?php echo $editAnimals->pod_number;?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">Injections:</label>
                                        </div>
                                        <div class="col-sm-4 col-12">
                                            <div class="form-group" style="margin-bottom: 25px;">
                                                <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">neuter/spay</label>
                                                <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                    class="form-control" type="text" name="1st" value="<?php echo $editAnimals->neuter_date;?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-12">
                                            <div class="form-group" style="margin-bottom: 25px;">
                                                <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">1<sup>ND</sup></label>
                                                <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                    class="form-control" type="text" name="1st" value="<?php echo $editAnimals->first_injection;?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-12">
                                            <div class="form-group" style="margin-bottom: 25px;">
                                                <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">2<sup>ND</sup></label>
                                                <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                    class="form-control" type="text" name="2st" value="<?php echo $editAnimals->second_injection;?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-12">
                                            <div class="form-group" style="margin-bottom: 25px;">
                                                <label style="color: #403e3e;font-size: 20px;font-family: 'ProximaNova-Regular';margin-bottom: 5px;-webkit-text-stroke: 0.2px;display: block;">Animal
                                                    Code</label>
                                                <input style="background: transparent;height: 52px;border: 1px solid #dcdcdc;border-radius: 4px;padding: 0 15px;box-shadow: none;width: 100%;color: #524d4d;font-family: 'ProximaNova-Light';font-size: 16px;"
                                                    class="form-control" type="text" name="code" value="<?php echo $editAnimals->animal_code;?>">
                                            </div>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

       