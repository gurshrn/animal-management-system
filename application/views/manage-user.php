<?php

	$this->load->view('include/header');

	$this->load->view('include/navbar');



?>

        <section class="animal-profile manage-user">

            <div class="container">

                <h2>Current Users</h2>

                <div class="profile_content">

                    <div class="row">

                        <div class="col-12">

                            <div class="datalog-table">

                            	<form id="addUser" action="<?php echo site_url('add-user');?>" method="POST" autocomplete="off">

                            		<input type="hidden" class="table-name" value="user">

                            		<input type="hidden" class="get-list" value="get-user-list">

                            		<input type="hidden" class="delete-url" value="delete-user">

                            		<input type="hidden" id="editStatus" name="edit-status">
									
									<div class="master- mCustomScrollbar">
	                                
	                                <table class="table repeater" >

	                                    <thead>

	                                        <tr>

	                                            <th>Name</th>

	                                            <th>Location</th>

	                                            <th>User Code <input data-repeater-create type="button" value="Add"/></th>

	                                        </tr>

	                                    </thead>

	                                    <tbody data-repeater-list="group">

	                                    	<tr data-repeater-item>



												<td>

													<!-- <input type="hidden" class="delete-id" name="delete_id">

													<input type="checkbox" id="deleteCheckbox" name="is_delete" data-attr="manage-user">

													<input type="hidden" id="usersid" name="id">

													<input type="text" class="form-control alphabets" name="name" placeholder="Name" maxlength="15" required /> -->
													<div class="checkbox">
														<label>
															<input type="hidden" class="delete-id" name="delete_id">
															<input type="checkbox" id="deleteCheckbox" name="is_delete" data-attr="manage-user"><span class="new-checkbox"></span>
															<input type="hidden" id="usersid" name="id">
															
															
															<input type="text" class="form-control editUser alphabets" name="name" placeholder="Name" maxlength="15" required />
														</label>
													</div>

												</td>

												<td>

													<select class="form-control editUser" name="location_id" required>

														<option value="">Select Location...</option>

														<?php

															if(isset($locationList) && !empty($locationList))

															{

																foreach($locationList as $val)

																{

																	echo '<option value="'.$val->id.'">'.ucfirst($val->location_name).'</option>';

																}

															}



														?>

													</select>

												</td>

												<td>

													<input type="text" class="form-control editUser check-user-code onlyvalidNumber" name="user_code" placeholder="User Code" maxlength="13" required>
													<label for="user_code" class="error userCodeError"></label>

													<!--input data-repeater-delete type="button" value="Delete"/-->

												</td>

											</tr>







										</tbody>

	                            	</table>

</div>

                                <div class="button_submit">

                                	<button class="export delete" type="button">Delete</button>

                                    <button class="exit exit-user-btn" type="submit">Exit</button>

                                </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </section>

<?php $this->load->view('include/footer');?>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="delete-modal">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete user?</h4>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-primary yes-delete-user" id="modal-btn-si">Yes</button>

        <button type="button" class="btn btn-warning" data-dismiss="modal" id="modal-btn-no">No</button>

      </div>

    </div>

  </div>

</div>



</body>

</html>
