<?php
    $this->load->view('include/header');
    $this->load->view('include/navbar');
?>
    <section class="animal-profile manage-user manage-animal">
        <div class="container">
            <h2>Current Animals</h2>
            <div class="profile_content">
                <div class="row">

                    <input type="hidden" class="delete-url" value="delete-manage-animal">

                    <div class="col-12">
                        <div class="datalog-table">
                           <form method="POST">
							   <div class="animal- mCustomScrollbar">
                            <table class="table allResult">

                                <thead>
                                    <tr>
                                        <th class="sorting" sort_by="name" sort_type="ASC">Name</th>
                                        <th class="sorting" sort_by="location_name" sort_type="ASC">Location</th>
                                        <th class="sorting" sort_by="animal_code" sort_type="ASC">Animal Code</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if(isset($animal) && !empty($animal)){
                                            foreach($animal as $val){

                                                $getAnimalLocation = getAnimalLocation($val->location_id);

                                                /*if($getAnimalLocation != '')
                                                {
                                                    $locationName = $getAnimalLocation->location_name;

                                                }
                                                else
                                                {
                                                    $locationName = '';
                                                }*/

                                    ?>

                                                <tr>


                                                    <td>
														<div class="checkbox">
														<label>
															<input type="checkbox" id="deleteCheckbox" name="is_delete[]" data-attr="animal-profile" value="<?php echo $val->id;?>"><span class="new-checkbox"></span><?php echo $val->name;?></label>
													</div>
                                                    </td>
                                                    <td><?php echo ucfirst($val->location_name);?></td>
                                                    <td><?php echo $val->animal_code; ?></td>
                                                    <td><a class="edit_btn" href="<?php echo site_url().'edit-animal/'.$val->id;?>">Edit</a></td>
                                                </tr>


                                    <?php } } ?>


                                </tbody>

							</table>
											</div>
                            </form>

                            <div class="button_submit">
                                <button class="export delete" type="button">Delete</button>
                                <a href="<?php echo site_url('dashboard');?>"><button class="exit action-btn" data-attr="Exit manage animal" type="button">Exit</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php $this->load->view('include/footer');?>

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="delete-modal">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header">

        <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete animal?</h4>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-primary yes-delete-user" id="modal-btn-si">Yes</button>

        <button type="button" class="btn btn-warning" data-dismiss="modal" id="modal-btn-no">No</button>

      </div>

    </div>

  </div>

</div>


    </body>
</html>
