<table class="table" align="center">
    <thead>
        <tr>
            <th colspan="5">Details</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if(isset($datalog) && !empty($datalog))
            { 
                foreach($datalog as $val)
                {
                    $userdata = getUserData($val->user_id);
                    if($val->table_id != 0)
                    {
                        $logdata = getLogId($val->table_id,$val->table_name);
                        
                        $code = $logdata->code;
                    }
                    else
                    {
                        $code = '';
                    }
                    
                    if($userdata->role == 'superadmin')
                    {
                        $userid = 'Master User';
                    }
                    else
                    {
                        $userid = 'USER'.' '.$userdata->user_code;
                    }
                    echo '<tr>';
                    echo '<td>'.date('d/m/Y',strtotime($val->created_at)).'</td>';
                    echo '<td>'.date('H:i',strtotime($val->created_at)).'</td>';
                    echo '<td>'.$userid.'</td>';
                    echo '<td>-</td>';
                    echo '<td>'.ucfirst($val->action).' '.$code.'</td>';
                    echo '</tr>';
                }
            }
            else
            {
                echo '<tr><td>No record found...</td></tr>';
            }
        ?>
    </tbody>
</table>