<table class="table" align="center">
    <thead>
        <tr>
            <th colspan="3">Weight record</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            if(isset($weightList) && !empty($weightList))
            {
                foreach($weightList as $val)
                {
                    echo '<tr>';
                    echo '<td>'.$val->created_at.'</td>';
                    echo '<td>-</td>';
                    echo '<td>'.number_format($val->weight,2).'</td>';
                    echo '</tr>';
                }
            }
            else
            {
                echo '<tr><td colspan="3">No record found...</td></tr>';
            }

        ?>
    </tbody>
</table>