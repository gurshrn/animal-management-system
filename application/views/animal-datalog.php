<?php
    $this->load->view('include/header');
    $this->load->view('include/navbar');
    $animalId = $this->uri->segment(2);
?>
        <section class="animal-datalog">
            <div class="container">
                <h2>Animal datalog</h2>
                <div class="datalog-table">
					<div class="animal- mCustomScrollbar">
						<table class="table">
							<thead>
								<tr>
									<th colspan="5">Details</th>
								</tr>
							</thead>
							<tbody>
								<?php
									if(isset($animaldatalog) && !empty($animaldatalog))
									{
										foreach($animaldatalog as $val)
										{
											$userdata = getUserData($val->user_id);
											if($val->table_id != 0)
											{
												$animalData = getAnimalData($val->table_id);
												$animalCode = $animalData->animal_code;
											}
											else
											{
												$animalCode = '';
											}

											if($userdata->role == 'superadmin')
											{
												$userid = 'Master User';
											}
											else
											{
												$userid = 'USER'.' '.$userdata->user_code;
											}
											echo '<tr>';
											echo '<td>'.date('d/m/Y',strtotime($val->created_at)).'</td>';
											echo '<td>'.date('H:i',strtotime($val->created_at)).'</td>';
											echo '<td>'.$userid.'</td>';
											echo '<td>-</td>';
											echo '<td>'.ucfirst($val->action).' '.$animalCode.'</td>';
										}
									}
									else
									{
										echo '<tr><td>No record found.</td></tr>';
									}
								?>
							</tbody>
						</table>
					</div>
                    <div class="button_submit">
                        <input type="hidden" value="animal_profile" class="tablename">
                        <input type="hidden" value="<?php echo $animalId;?>" class="tableid">

                        <a href="<?php echo site_url().'export-animaldatalog/'.$animalId;?>"><button class="export action-btn" type="button" data-attr="Export animal datalog">Export</button></a>

                        <button class="exit action-btn" data-target="<?php echo 'edit-animal/'.$animalId;?>" data-attr="Exit animal datalog" type="button">Exit</button>
                    </div>
                </div>
            </div>
        </section>

<?php $this->load->view('include/footer');?>

    </body>

</html>
