<thead>
    <tr>
        <th class="sorting" sort_by="name" sort_type="ASC">Name</th>
        <th class="sorting" sort_by="location_name" sort_type="ASC">Location</th>
        <th class="sorting" sort_by="animal_code" sort_type="ASC">Animal Code</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    <?php
        if(isset($animal) && !empty($animal))
        {
            foreach($animal as $val)
            {

                $getAnimalLocation = getAnimalLocation($val->location_id);

                if($getAnimalLocation != '')
                {
                    $locationName = $getAnimalLocation->location_name;

                }
                else
                {
                    $locationName = '';
                }

    ?>

                <tr>


                    <td>
						<div class="checkbox">
						<label>
							<input type="checkbox" id="deleteCheckbox" name="is_delete[]" data-attr="animal-profile" value="<?php echo $val->id;?>"><span class="new-checkbox"></span><?php echo $val->name;?></label>
					</div>
                    </td>
                    <td><?php echo ucfirst($locationName);?></td>
                    <td><?php echo $val->animal_code; ?></td>
                    <td><a class="edit_btn" href="<?php echo site_url().'edit-animal/'.$val->id;?>">Edit</a></td>
                </tr>


    <?php } } ?>


</tbody>
