<?php
    $this->load->view('include/header');
    $this->load->view('include/navbar');
?>
    <section class="animal-profile comment master-datalog">
        <div class="container">
            <h2>Master datalog</h2>
            <div class="profile_content">
                <div class="row">
                    <div class="col-12">
                        <div class="datalog-table">
							<div class="scroll- mCustomScrollbar">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="5">Details</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if(isset($datalog) && !empty($datalog))
                                        {
                                            foreach($datalog as $val)
                                            {
                                                $userdata = getUserData($val->user_id);
                                                if($val->table_id != 0)
                                                {
                                                    $logdata = getLogId($val->table_id,$val->table_name);

                                                    $code = $logdata->code;
                                                }
                                                else
                                                {
                                                    $code = '';
                                                }

                                                if(!empty($userdata))
                                                {
                                                     if($userdata->role == 'superadmin')
                                                    {
                                                        $userid = 'Master User';
                                                    }
                                                    else
                                                    {
                                                        $userid = 'USER'.' '.$userdata->user_code;
                                                    }
                                                }
                                                else
                                                {
                                                    $userid = $val->user_id;
                                                }



                                                echo '<tr>';
                                                echo '<td>'.date('d/m/Y',strtotime($val->created_at)).'</td>';
                                                echo '<td>'.date('H:i',strtotime($val->created_at)).'</td>';
                                                echo '<td>'.$userid.'</td>';
                                                echo '<td>-</td>';
                                                echo '<td>'.ucfirst($val->action).' '.$code.'</td>';
                                                echo '</tr>';
                                            }
                                        }
                                        else
                                        {
                                            echo '<tr><td>No record found...</td></tr>';
                                        }
                                    ?>
                                </tbody>
							</table>
							</div>
                            <div class="button_submit">

                                <a href="<?php echo site_url('export-master-datalog');?>"><button class="export action-btn" data-attr="Export master datalog" type="button">Export</button></a>

                                <a href="<?php echo site_url('dashboard');?>"><button class="exit action-btn" data-attr="Exit master datalog" type="button">Exit</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('include/footer');?>
