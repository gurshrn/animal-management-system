<?php $this->load->view('include/header');?>

    <div class="preloader" style="display:none;">
        <div class="loader"></div>
    </div>

    <body>

        <div class="container">

            <div class="login_image">

                <div class="login_form">

                    <div class="logged">

                        <a href="#">

                            <img src="<?php echo site_url(); ?>assets/images/logo.png" alt="Logo">

                        </a>

                        <div class="login_content">

                            <form id="user-authenticate" method="post" action="<?php echo site_url('/');?>" autocomplete="off">

                                <div class="row">

                                    <div class="col-sm-6 col-12">

                                        <div class="form-group">

                                            <label>User</label>

                                            <input class="form-control onlyvalidNumber" type="text"  name="user" placeholder="1234567891112" maxlength="13" required>

                                        </div>

                                    </div>

                                    <div class="col-sm-6 col-12">

                                        <div class="form-group">

                                            <label>Animal Profile</label>

                                            <input class="form-control onlyvalidNumber" type="text" name="animal_code" placeholder="1234567891112" maxlength="13">

                                        </div>

                                    </div>

                                    <div class="button_submit">

                                        <button class="exit" type="submit">authenticate</button>

                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

<?php $this->load->view('include/footer');?>