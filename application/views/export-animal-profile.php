<?php 
    $editAnimals = $editAnimal[0];
?>
<section class="animal-profile">
		<div class="container">
			<h2>Animal Profile</h2>
			<div class="profile_content">
				<form id="add-animal" method="post" action="<?php echo site_url('add-animal');?>" autocomplete="off" enctype="multipart/form-data">

				<div class="row">
					<div class="col-sm-3 col-12 profile_wdth">
						<div class="pic">
							<div class="circle">
								<img class="profile-pic" src="<?php echo site_url(); ?>assets/images/profile.jpg" alt="Profile">
							</div>
							<div class="p-image">
								<i class="fa fa-camera upload-button"></i>
								<input class="file-upload" type="file" name="images" accept="jpeg,png,jpg" required/>
							</div>
						</div>
					</div>
					<div class="col-sm-9 col-12 content_wdth">
						<div class="animal_content">

								<input type="hidden" class="animalEditId" name="id" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->id;}?>">

								<div class="row">
									<div class="col-sm-6 col-12">
										<div class="form-group">
										<label>Name</label>
										<input class="form-control firstCap alphabets update-animal-profile" type="text" name="name" placeholder="Fluffy" maxlength="15" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->name;}?>" required>
										</div>
									</div>
									<div class="col-sm-6 col-12">
										<div class="form-group">
											<label>Location</label>
											<select class="form-control update-animal-profile" name="location_id" required>
												<?php
													if(isset($location) && !empty($location)){
														foreach($location as $vals){
												?>
													<option value="<?php echo $vals->id; ?>" selected=""><?php echo ucfirst($vals->location_name);?></option>

												<?php } }?>
											</select>
										</div>
									</div>

									<div class="col-sm-6 col-12">
										<div class="form-group">
											<label>Status</label>
												<select class="form-control update-animal-profile" name="status_id">
													<?php
														if(isset($status) && !empty($status)){
															foreach($status as $val){
													?>
														<option value="<?php echo $val->id; ?>" selected="" style="background-color:<?php echo $val->color;?>"><?php echo $val->status_name;?></option>

													<?php } }?>
												</select>
										</div>
									</div>

									<div class="col-sm-6 col-12">
										<div class="row">
											<div class="col-5">
												<div class="form-group">
													<label>Date of Birth</label>
													<input class="form-control datepicker birth-date update-animal-profile" type="text" name="date_birth" placeholder="01/05/2017" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->date_birth;}?>" readonly required>
												</div>
											</div>
											<div class="col-7 year_wdth">
												<div class="form-group">
													<label>Age</label>
													<div class="year">
														<input class="form-control update-animal-profile" type="text" name="year" placeholder="1 Year">
													</div>
													<div class="month">
														<input class="form-control" type="text" name="month" placeholder="03 Month">
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-sm-12 col-12">
										<div class="row">
											<div class="col-sm-4 col-12">
												<div class="form-group">
													<label>Date Arrived</label>
													<input class="form-control datepicker update-animal-profile" type="text" name="date_arrived" placeholder="12/08/2018" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->date_arrived;}?>" readonly required>
												</div>
											</div>

											<div class="col-sm-4 col-12">
												<div class="form-group">
													<label>Chip Number</label>
													<input class="form-control onlyvalidNumber update-animal-profile" type="text" name="chip_number" placeholder="1233454567899" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->chip_number;}?>" maxlength="15">
												</div>
											</div>
											<div class="col-sm-4 col-12">
												<div class="form-group">
													<label>POD Number</label>
													<input class="form-control onlyvalidNumber update-animal-profile" type="text" name="pod_number" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->pod_number;}?>" placeholder="17" maxlength="2">
												</div>
											</div>
										</div>
									</div>

									<div class="col-12">
										<label>Injections:</label>
									</div>
									<div class="col-sm-4 col-12">
                                            <div class="form-group">
                                                <label>neuter/spay</label>
                                                <input class="form-control datepicker update-animal-profile" type="text" name="neuter_date" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->neuter_date;}?>" placeholder="12/08/2018"  readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-12">
                                            <div class="form-group">
                                                <label>1<sup>ND</sup></label>
                                                <input class="form-control datepicker update-animal-profile" type="text" name="first_injection" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->first_injection;}?>" placeholder="12/08/2018" readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-12">
                                            <div class="form-group">
                                                <label>2<sup>ND</sup></label>
                                                <input class="form-control datepicker update-animal-profile" type="text" name="second_injection" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->second_injection;}?>"  placeholder="12/08/2018" readonly>
                                            </div>
                                        </div>

									<div class="col-sm-6 col-12">
										<div class="form-group">
											<label>Animal Code</label>
											<input class="form-control onlyvalidNumber update-animal-profile" type="text" name="animal_code" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->animal_code;}?>" placeholder="1234567895648" maxlength="13" required>
										</div>
									</div>

									<div class="col-sm-6 col-12">
										<div class="button_submit">

											<button class="exit" type="<?php if(isset($editAnimals) && !empty($editAnimals))
								{ echo "button";}else{ echo "submit";} ?>">Exit</button>
										</div>
									</div>
								</div>
								</form>
							</div>

							<?php
								if(isset($editAnimals) && !empty($editAnimals))
								{
							?>

									<div class="all_btn">
										<div class="button_submit">
											<input type="hidden" value="animal_profile" class="tablename">
											<input type="hidden" value="<?php echo $editAnimals->id;?>" class="tableid">

											<button class="exit action-btn" data-target="<?php echo 'weight/'.$editAnimals->id;?>" data-attr="Viewed animal weight" type="button">Weight</button>
										</div>
										<div class="button_submit">
											<button class="exit action-btn" data-target="<?php echo 'animal-history/'.$editAnimals->id;?>" data-attr="Viewed animal history" type="button">History</button>
										</div>

										<div class="button_submit">
											<button class="exit action-btn" data-target="<?php echo 'animal-datalog/'.$editAnimals->id;?>" data-attr="Viewed animal datalog" type="button">Animal datalog</button>
										</div>

										<div class="button_submit">
											<button class="exit action-btn export-profile" data-attr="Export animal profile" type="button">Export</button>
										</div>
									</div>
							<?php } ?>
						</div>
					</div>

			</div>
		</div>
	</section>
