<?php 

	$this->load->view('include/header.php');

	$this->load->view('include/navbar.php');

	

?>

	<section class="animal_section">

            <div class="container">

                <div class="row">

                   <div class="col-sm-4 col-12">

                        <div class="animal">

                            <a class="action-btn" data-attr="Create New Animal" href="<?php echo site_url('animal-profile');?>">

                                <figure>

                                    <img src="<?php echo site_url(); ?>assets/images/animal-1.png" alt="Animal">

                                </figure>

                                Create New Animal
                            </a>

                        </div>

                   </div>

                    <div class="col-sm-4 col-12">

                        <div class="animal">

                            <a class="action-btn" data-attr="Manage User" href="<?php echo site_url('manage-users');?>">

                                <figure>

                                    <img src="<?php echo site_url(); ?>assets/images/animal-2.png" alt="Animal-2">

                                </figure>

                                Manage Users
                            </a>

                        </div>

                    </div>

                    <div class="col-sm-4 col-12">

                        <div class="animal">

                            <a class="action-btn" data-attr="Manage Location" href="<?php echo site_url('manage-location');?>">

                                <figure>

                                    <img src="<?php echo site_url(); ?>assets/images/animal-3.png" alt="Animal-3">

                                </figure>

                                Manage Location
                            </a>

                        </div>

                    </div>

                    <div class="col-sm-4 col-12">

                        <div class="animal">

                            <a class="action-btn" data-attr="Manage animal" href="<?php echo site_url('manage-animal');?>">

                                <figure>

                                    <img src="<?php echo site_url(); ?>assets/images/animal-4.png" alt="Animal-4">

                                </figure>

                                Manage Animal
                            </a>

                        </div>

                    </div>

                    <div class="col-sm-4 col-12">

                        <div class="animal">

                            <a class="action-btn" data-attr="Master Datalog" href="<?php echo site_url('master-datalog');?>">

                                <figure>

                                    <img src="<?php echo site_url(); ?>assets/images/animal-5.png" alt="Animal-5">

                                </figure>

                                Master Data Log
                            </a>

                        </div>

                    </div>

                </div>

            </div>

        </section>

<?php $this->load->view('include/footer.php');?>

		

	

       

      