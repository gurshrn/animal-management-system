<?php
	$this->load->view('include/header');
	$this->load->view('include/navbar');

	if(isset($editAnimal) && !empty($editAnimal))
	{
		$editAnimals = $editAnimal[0];
		$statusId = $editAnimals->status_id;

	}
	else
	{
		$selectedStatus = getSettings('selected_status');
		$statusId = $selectedStatus->value;
	}
	$getStatusColor = getStatusColor($statusId);
	$userRole = $this->session->userdata('role');
?>
	<section class="animal-profile">
		<div class="container">
			<h2>Animal Profile</h2>
			<div class="profile_content">
				<?php if(!isset($editAnimals)){ ?>

					<form id="add-animal" method="post" action="<?php echo site_url('add-animal');?>" autocomplete="off" enctype="multipart/form-data">
				<?php } ?>

				<div class="row">
					<div class="col-sm-3 col-12 profile_wdth">
						<div class="pic">
							<div class="circle">
								<img class="profile-pic" src="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo site_url().'assets/upload/'.$editAnimals->image;} else{ echo site_url().'assets/images/profile.jpg';}?>" alt="Profile">
							</div>
							<?php
								if($userRole != 'superadmin' && !isset($editAnimals))
								{
									echo '<form action="'.site_url('update-animal-profile').'" method="POST" id="formAvatar" accept-charset="UTF-8" enctype="multipart/form-data">
											<div class="p-image">
												<i class="fa fa-camera upload-button"></i>
												<input class="file-upload" id="imageUpload" type="file" name="images"  required/>
											</div>
										</form>';
								}
							 	else if($userRole == 'superadmin')
							 	{
									if(isset($editAnimals) && !empty($editAnimals))
									{

								?>
										<form action="<?php echo site_url('update-animal-profile');?>" method="POST" id="formAvatar" accept-charset="UTF-8" enctype="multipart/form-data">

											<input type="hidden" class="animalEditId" name="editId" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->id;}?>">
											<div class="p-image">
												<i class="fa fa-camera upload-button"></i>
												<input class="file-upload" id="imageUpload" type="file" name="images" accept=".png, .jpg, .jpeg"  required/>
											</div>
										</form>
								<?php }  else { ?>

								<div class="p-image">
									<i class="fa fa-camera upload-button"></i>
									<input type="hidden" name="uploadImage" id="uploadImageProfile" required>
									<input class="file-upload" id="profileUploadImage" type="file" name="images"/>
								</div>

							<?php } } ?>
						</div>
					</div>
					<div class="col-sm-9 col-12 content_wdth">
						<div class="animal_content">

								<input type="hidden" class="animalEditId" name="id" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->id;}?>">

								<div class="row">
									<div class="col-sm-4 col-12">
										<div class="form-group">
										<label>Name</label>

										<input class="form-control firstCap alphabets <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" type="text" <?php if($userRole == 'superadmin' || ($userRole != 'superadmin' && !isset($editAnimals))){ echo 'name="name"';} ?> placeholder="Fluffy" maxlength="15" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->name;}?>" required <?php if($userRole != 'superadmin' && isset($editAnimals)){ echo 'disabled';} ?>>
										</div>
									</div>
									<div class="col-sm-4 col-12">
										<div class="form-group">
											<label>Location</label>

											<select class="form-control <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" <?php if($userRole == 'superadmin' || ($userRole != 'superadmin' && !isset($editAnimals))){ echo 'name="location_id"' ; } ?> required <?php if($userRole != 'superadmin' && isset($editAnimals)){ echo 'disabled';} ?>>

												<option value="">Select location...</option>
												<?php
													if(isset($location) && !empty($location))
													{
														foreach($location as $vals)
														{
															if(isset($editAnimals) && !empty($editAnimals))
															{
																if($editAnimals->location_id == $vals->id)
																{
																	$sel = 'selected="selected"';
																}
																else
																{
																	$sel = '';
																}
															}
															else
															{
																$sel = '';
															}

															echo '<option value="'.$vals->id.'" '.$sel.'>'.ucfirst($vals->location_name).'</option>';
														}
													}
												?>
											</select>
										</div>
									</div>

									<div class="col-sm-4 col-12">
										<div class="form-group">
											<label>Status</label>
												<select class="animal-status form-control <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" name="status_id" <?php echo 'style="background-color:'.$getStatusColor->color.'"';?>">
													<?php
														if(isset($status) && !empty($status))
														{
															foreach($status as $val)
															{
																if(isset($editAnimals) && !empty($editAnimals))
																{
																	if($editAnimals->status_id == $val->id)
																	{
																		$sel = 'selected="selected"';
																	}
																	else
																	{
																		$sel = '';
																	}
																}
																else
																{

																	$sel = '';
																}
													?>
														<option value="<?php echo $val->id; ?>" <?php echo $sel;?> style="background-color:<?php echo $val->color;?>"><?php echo $val->status_name;?></option>

													<?php } }?>
												</select>
										</div>
									</div>

									<div class="col-sm-12 col-12">
										<div class="row">
											<div class="col-sm-4 col-12">
												<div class="form-group">
													<label>Date of Birth</label>
													<?php if(isset($editAnimals) && $userRole != 'superadmin') {?>
														<input type="hidden" class="dobDatepicker" value="<?php echo  $editAnimals->date_birth;?>">
													<?php } ?>
													<input class="form-control <?php if($userRole == 'superadmin'){ echo 'dobDatepicker';} if($userRole != 'superadmin' && !isset($editAnimals)){ echo 'dobDatepicker';}?> birth-date <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" type="text"  <?php if($userRole == 'superadmin' || ($userRole != 'superadmin' && !isset($editAnimals))){ echo 'name="date_birth"'; } ?>placeholder="01/05/2017" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->date_birth;}?>" readonly required>
												</div>
											</div>
											<div class="col-sm-8 col-12">
												<label>Age</label>
												<div class="row">
													<div class="col-sm-4 col-12">
														<div class="form-group">
															<input class="form-control ageYear" type="text" name="year" placeholder="1 Year" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->ageYear;}?>" readonly>
														</div>
													</div>
													<div class="col-sm-4 col-12">
														<div class="form-group">
															<input class="form-control ageMonth" type="text" name="month" placeholder="03 Month" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->ageMonth;}?>" readonly>
														</div>
													</div>
													<div class="col-sm-4 col-12">
														<div class="form-group">
															<input class="form-control ageDay" type="text" name="day" placeholder="03 day" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->ageDay;}?>" readonly>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-sm-12 col-12">
										<div class="row">
											<div class="col-sm-4 col-12">
												<div class="form-group">
													<label>Date Arrived</label>
													<input class="form-control  <?php if($userRole == 'superadmin'){ echo 'dateArrived';} if($userRole != 'superadmin' && !isset($editAnimals)){ echo 'dateArrived';}?> <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" type="text" <?php if($userRole == 'superadmin' || ($userRole != 'superadmin' && !isset($editAnimals))){ echo 'name="date_arrived"';}?> placeholder="12/08/2018" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->date_arrived;}?>" readonly required>
												</div>
											</div>

											<div class="col-sm-4 col-12">
												<div class="form-group">
													<label>Chip Number</label>
													<input class="form-control onlyvalidNumber <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" type="text" <?php if($userRole == 'superadmin' || ($userRole != 'superadmin' && (!isset($editAnimals) || $editAnimals->chip_number == ''))){ echo 'name="chip_number"';}?> placeholder="1233454567899" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->chip_number;}?>" maxlength="15" <?php if($userRole != 'superadmin' && isset($editAnimals) && $editAnimals->chip_number != ''){ echo 'disabled';}?>>
												</div>
											</div>
											<div class="col-sm-4 col-12">
												<div class="form-group">
													<label>POD Number</label>
													<input class="form-control onlyvalidNumber <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" type="text" name="pod_number" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->pod_number;}?>" placeholder="17" maxlength="2">
												</div>
											</div>
										</div>
									</div>

									<div class="col-12">
										<label>Injections:</label>
									</div>
									<div class="col-sm-4 col-12">
                                            <div class="form-group">
                                                <label>neuter/spay</label>
                                                <input class="form-control <?php if($userRole == 'superadmin'){ echo 'neuterDate';} if($userRole != 'superadmin' && (!isset($editAnimals) || $editAnimals->neuter_date == '')){ echo 'neuterDate';}?> <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" type="text" <?php if($userRole == 'superadmin' || ($userRole != 'superadmin' && (!isset($editAnimals) || $editAnimals->neuter_date == ''))){ echo 'name="neuter_date"';}?> value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->neuter_date;}?>" placeholder="12/08/2018"  readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-12">
                                            <div class="form-group">
                                                <label>1<sup>ND</sup></label>
                                                <?php if(isset($editAnimals) && $userRole != 'superadmin' && $editAnimals->first_injection != ''){ ?>
                                                <input type="hidden" class="datepicker1" value="<?php echo $editAnimals->first_injection;?>">
                                            <?php } ?>
                                                <input class="form-control <?php if($userRole == 'superadmin'){ echo 'datepicker1';} if($userRole != 'superadmin' && (!isset($editAnimals) || $editAnimals->first_injection == '')){ echo 'datepicker1';}?> <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" type="text" <?php if($userRole == 'superadmin' || ($userRole != 'superadmin' && (!isset($editAnimals) || $editAnimals->first_injection == ''))){ echo 'name="first_injection"';}?> value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->first_injection;}?>" placeholder="12/08/2018" readonly>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-12">
                                            <div class="form-group">
                                                <label>2<sup>ND</sup></label>
                                                <input class="form-control <?php if($userRole == 'superadmin'){ echo 'datepicker2';}if($userRole != 'superadmin' && (!isset($editAnimals) || $editAnimals->second_injection == '')){ echo 'datepicker2';}?> <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" type="text" <?php if($userRole == 'superadmin' || ($userRole != 'superadmin' && (!isset($editAnimals) || $editAnimals->second_injection == ''))){ echo 'name="second_injection"';}?> value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->second_injection;}?>"  placeholder="12/08/2018" readonly>
                                            </div>
                                        </div>

									<div class="col-sm-6 col-12">
										<div class="form-group">
											<label>Animal Code</label>
											<input class="form-control onlyvalidNumber <?php if(isset($editAnimals) && !empty($editAnimals)){ echo 'update-animal-profile';}?>" type="text" <?php if($userRole == 'superadmin' || ($userRole != 'superadmin' && !isset($editAnimals))){ echo 'name="animal_code"';}?> value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->animal_code;}?>" placeholder="1234567895648" maxlength="13" required <?php if($userRole != 'superadmin' && isset($editAnimals)){ echo 'disabled';}?>>
										</div>
									</div>

									<div class="col-sm-6 col-12">
										<div class="button_submit">
											<?php
												if(isset($editAnimals) && !empty($editAnimals))
												{
												    if($userRole == 'superadmin')
												    {
												        $site_url = site_url('dashboard');
												    }
												    else
												    {
												        $site_url = site_url('/');
												    }

													echo '<a href="'.$site_url.'"><button class="exit action-btn" type="button" data-attr="Exit update animal profile">Exit</button></a>';
												}
												else
												{
													echo '<button class="exit exit-profile" type="submit">Exit</button>';
												}

										 	?>


										</div>
									</div>
								</div>
								<?php if(!isset($editAnimals)){ ?> </form> <?php } ?>
							</div>

							<?php
								if(isset($editAnimals) && !empty($editAnimals))
								{
							?>

									<div class="all_btn">
										<div class="button_submit">
											<input type="hidden" value="animal_profile" class="tablename">
											<input type="hidden" value="<?php echo $editAnimals->id;?>" class="tableid">

											<button class="exit action-btn" data-target="<?php echo 'weight/'.$editAnimals->id;?>" data-attr="Viewed animal weight" type="button">Weight</button>
										</div>
										<div class="button_submit">
											<button class="exit action-btn" data-target="<?php echo 'animal-history/'.$editAnimals->id;?>" data-attr="Viewed animal history" type="button">History</button>
										</div>

										<?php
										    if($userRole == 'superadmin')
										    {
										 ?>

        										<div class="button_submit">
        											<button class="exit action-btn" data-target="<?php echo 'animal-datalog/'.$editAnimals->id;?>" data-attr="Viewed animal datalog" type="button">Animal datalog</button>
        										</div>

        							    <?php } ?>

										<div class="button_submit">
											<button class="exit action-btn export-profile" data-attr="Export animal profile" type="button">Export</button>
										</div>
									</div>
							<?php } ?>
						</div>
					</div>

			</div>
		</div>
	</section>

<?php $this->load->view('include/footer');?>

	<div class="modal fade" id="export-animal-profile" role="dialog">
	    <div class="modal-dialog">

	     	<div class="modal-content">
		        <div class="modal-header">
		          <h4 class="modal-title">Export Animal Profile</h4>
		        </div>
		        <form id="export-profile" method="post" action="<?php echo site_url('export-animal-profile');?>">
		        	<input type="hidden" name="id" value="<?php if(isset($editAnimals) && !empty($editAnimals)){ echo $editAnimals->id;}?>">
			        <div class="modal-body">
			          	<select class="form-control exportDoc" name="doc_type">
				          	<option value="">Select Type...</option>
				          	<option value="pdf">PDF</option>
				      		<option value="doc">DOC</option>
			          	</select>
			        </div>
			    </form>
		        <div class="modal-footer">
		          	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        </div>
	      	</div>
	    </div>
  	</div>
	</body>
</html>
