<?php

	if (!defined('BASEPATH'))
    exit('No direct script access allowed');

	function getUserData($userid = null) 
	{
	    $ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('user');
	    $ci->db->where('id', $userid);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getSettings($maxWeight) 
	{
	    $ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('settings');
	    $ci->db->where('name',$maxWeight);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getAnimalData($id)
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('animal_profile');
	    $ci->db->where('id',$id);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getLogId($tableId,$tableName)
	{
		$ci = & get_instance();
		if($tableName == 'animal_profile')
		{
			$ci->db->select('animal_code as code');
		}
		else
		{
			$ci->db->select('user_code as code');
		}
		$ci->db->from($tableName);
	    $ci->db->where('id',$tableId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getAllAssociatedEmails($locationId)
	{
		$ci = & get_instance();
		$ci->db->select('email');
	    $ci->db->from('location_associated_email');
	    $ci->db->where('location_id',$locationId);
	    $ci->db->where('deleted_at IS NULL', null, true);
	    $query = $ci->db->get();
	    $result = $query->result();
	    
	    if(!empty($result))
	    {
	    	foreach($result as $val)
		    {
		        $emails[] = $val->email;
		    }
		   $locationEmail = implode(",",$emails);
	    }
	    else
	    {
	    	$locationEmail = '';
	    }
	    
	    return $locationEmail;
	}
	function getStatusColor($id)
	{
		$ci = & get_instance();
		$ci->db->select('color');
		$ci->db->from('animal_status');
	    $ci->db->where('id',$id);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getAnimalLocation($locationId)
	{
		$ci = & get_instance();
		$ci->db->select('location_name');
		$ci->db->from('manage_location');
	    $ci->db->where('id',$locationId);
	    $ci->db->where('deleted_at IS NULL', null, true);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function getObs($animalId)
	{
		$ci = & get_instance();
		$ci->db->select('*');
	    $ci->db->from('weight_observation');
	    $ci->db->where('status','0');
	    $ci->db->where('animal_id',$animalId);
	    $query = $ci->db->get();
	    $result = $query->row();
	    return $result;
	}
	function sendEmail($email) 
	{
	    $ci = & get_instance();
        $ci->load->library('email');

        $ci->email->initialize(array(
          'protocol' => 'smtp',
          'smtp_host' => 'smtp.sendgrid.net',
          'smtp_user' => 'gurjeevan',
          'smtp_pass' => 'im@rk123#@',
          'smtp_port' => 587,
          'crlf' => "\r\n",
          'newline' => "\r\n"
        ));
                
        $ci->email->from('animal@gmail.com', 'Animal Management');
        $ci->email->to($email['emailto']); 

        $ci->email->subject($email['subject']);
        $ci->email->message($email['message']);
        $result = $ci->email->send();
        return $result;
    }
	

?>