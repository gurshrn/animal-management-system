<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class AnimalController extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function index($id = NULL)
	{
		$userId = $this->session->userdata('userId');
		$userRole = $this->session->userdata('role');
		if($userId == '')
		{
			redirect(site_url('/'));
		}
		else
		{

			$data['status'] = $this->animal->getStatus();
			$data['location'] = $this->user->getLocationListForAnimal();
			if($id != '')
			{
				$data['editAnimal'] = $this->animal->getAnimalList($id);
			}
			$this->load->view('animal-profile',$data);
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				if (!empty($_FILES['images']['size'])) 
				{
					$count = count($_FILES['images']['size']);
					$path = 'assets/upload/';
					$imagename = $_FILES['images'];
					$date = time();
					$exps = explode('.', $imagename['name']);
					
					$_FILES['images']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
					$_FILES['images']['type'] = $imagename['type'];
					$_FILES['images']['tmp_name'] = $imagename['tmp_name'];
					$_FILES['images']['error'] = $imagename['error'];
					$_FILES['images']['size'] = $imagename['size'];
					
					$config['upload_path'] = $path;
					$config['allowed_types'] = '*';
					$config['max_size'] = '100000';
					$config['max_width'] = '100024';
					$config['max_height'] = '100768';
					
					$this->load->library('upload', $config);
					$this->upload->do_upload('images');
					$datas = $this->upload->data();
					$name_array = $datas['file_name'];
					$imageName = $name_array;
				}
				$result = $this->animal->addAnimal($_POST,$imageName);
				if(isset($result['success']))
				{
					$response['success'] = true;
		            $response['success_message'] = $result['success_message'];
		            if($userRole == 'user')
		            {
		            	$response['url'] = site_url('/');
		            }
		            else
		            {
		            	$response['url'] = site_url('dashboard');
		            }
		            
				}
				else
				{
					$response['success'] = false;
		            $response['success_message'] = $result['error_message'];
		        }
		        echo json_encode($response);exit;
			}
		}
	}

/*==============Manage animal===============*/

	public function manageAnimal()
	{
		$userId = $this->session->userdata('userId');
		$userRole = $this->session->userdata('role');
		if($userId == '' || $userRole != 'superadmin')
		{
			redirect(site_url('/'));
		}
		else
		{
			$data['animal'] = $this->animal->getAnimalList();
			$this->load->view('manage-animal',$data);
		}
	}
	public function deleteManageAnimal()
	{
		$tableName = 'animal_profile';
		$data = $this->user->commonDelete($_POST,$tableName);
		if(isset($data['success']))
		{
			$param = array(
				'actionPerform' => 'Delete animal',
				'tablename' => '',
				'tableId' => '',
			);
			$this->animal->masterDatalog($param);
			$result['success'] = $data['success'];
		}
		else
		{
			$result['success'] = $data['error'];
		}
		echo json_encode($result);exit;	
	}

/*=============Manage weight===========*/

	public function manageWeight($id = NULL)
	{
		$data['weightList'] = $this->animal->getWeightList($id);
		$this->load->view('weight',$data);
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$maxWeightDiff = getSettings('weight_difference');
			$weightDiff = $maxWeightDiff->value;
			
			$previousWeight = $this->animal->getPreviousWeight($_POST['animalId']);
			if(!empty($previousWeight))
			{
				$diff = abs($previousWeight->weight-$_POST['animalWeight']);
			}
			
			$result = $this->animal->addAnimalWeight($_POST); 
			if(isset($result['success']))
			{
				$param = array(
					'actionPerform' => 'Add animal weight',
					'tablename' => 'animal_profile',
					'tableId' => $_POST['animalId'],
				);
				$this->animal->masterDatalog($param);
				if(!empty($previousWeight))
				{
					if($diff >= $weightDiff && $_POST['animalWeight'] > $previousWeight->weight)
					{
					    $message = 'Weight difference between previous weight and current weigth is '.$weightDiff;
						$this->sendemail($_POST['animalId'],$message);
					}
					if($_POST['animalWeight'] < $previousWeight->weight)
					{
					    $message = 'Enter weight is less then previous weight';
						$this->sendemail($_POST['animalId'],$message);
					}
				}
				else
				{
					if($_POST['animalWeight'] >= 2)
					{
					    $message = 'Enter Weight is greater than or equal to 2';
						$this->sendemail($_POST['animalId'],$message);
					}
				}
				$response['success'] = $result['success'];
			}
			else
			{
				$response['success'] = $result['error'];
			}
			echo json_encode($response);exit;
		}
	}
	public function sendemail($animal_id,$message)
	{
	    $animal=$this->animal->getAnimalList($animal_id);
		$locationEmail = getAllAssociatedEmails($animal[0]->location_id); 
        $subject = 'Regarding Weight of '.$animal[0]->name."(".$animal[0]->animal_code.")"; 
        $variables = array(
            'subject' => $subject,
            'message' => $message,
            'emailto' => $locationEmail,
        );
        sendEmail($variables);
	}

/*===============OBS button functionlaity=============*/

	public function addWeightObs()
	{
		$result = $this->animal->addObs($_POST['animalId']);
		if(isset($result['success']))
		{
			$response['success'] = $result['success'];
		}
		else
		{
			$response['success'] = $result['error'];
		}
		echo json_encode($response);exit;
	}

/*=============Export animal weight==============*/

	public function exportAnimalWeight($id)
	{
		header("Content-Type: application/vnd.ms-word");
		header("Expires: 0");
		header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
		header("Content-disposition: attachment; filename=\"Animal Weight.doc\"");
		$data['weightList'] = $this->animal->getWeightList($id);
		$output = $this->load->view("export-weight",$data);
	}

/*================Manage animal history==============*/

	public function manageAnimalHistory($id = NULL)
	{
		$data['animalId'] = $id;
		$data['animalHistory'] = $this->animal->getAnimalHistory($id);
		$this->load->view('history',$data);
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$result = $this->animal->addAnimalHistory($_POST); 
			if(isset($result['success']))
			{
				$param = array(
					'actionPerform' => 'Add animal history',
					'tablename' => 'animal_profile',
					'tableId' => $_POST['animalId'],
				);
				$this->animal->masterDatalog($param);
				$userdata = getUserData($result['user_id']);
				if($userdata->role == 'superadmin')
	            {
	                $userid = 'Master User';
	            }
	            else
	            {
	                $userid = 'USER'.' '.$userdata->user_code;
	            }
				$response['type'] = $userid;
				$response['success'] = $result['success'];
			}
			else
			{
				$response['success'] = $result['error'];
			}
			echo json_encode($response);exit;
		}
	}

/*=============Export animal history===================*/

	public function exportAnimalHistory($id)
	{
		header("Content-Type: application/vnd.ms-word");
		header("Expires: 0");
		header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
		header("Content-disposition: attachment; filename=\"Animal History.doc\"");
		$data['animalHistory'] = $this->animal->getAnimalHistory($id);
		$output = $this->load->view("export-animal-history",$data);
	}

/*===============Manage animal datalog==============*/

	public function manageAnimalDataLog($id = NULL)
	{
		$data['animalId'] = $id;
		$data['animaldatalog'] = $this->animal->getAnimalDatalog($id);
		$this->load->view('animal-datalog',$data);
	}

/*=================Export animal datalog==============*/

	public function exportAnimalDatalog($id)
	{
		header("Content-Type: application/vnd.ms-word");
		header("Expires: 0");
		header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
		header("Content-disposition: attachment; filename=\"Animal DataLog.doc\"");
		$data['animaldatalog'] = $this->animal->getAnimalDatalog($id);
		$output = $this->load->view("export-animal-datalog",$data);
	}

/*==============Update animal Profile==============*/

	public function updateAnimalProfile()
	{
		if (isset($_FILES) && !empty($_FILES['images']['size'])) 
		{
			$count = count($_FILES['images']['size']);
			$path = 'assets/upload/';
			$imagename = $_FILES['images'];
			$date = time();
			$exps = explode('.', $imagename['name']);
			
			$_FILES['images']['name'] = $exps[0] . '_' . $date . "." . $exps[1];
			$_FILES['images']['type'] = $imagename['type'];
			$_FILES['images']['tmp_name'] = $imagename['tmp_name'];
			$_FILES['images']['error'] = $imagename['error'];
			$_FILES['images']['size'] = $imagename['size'];
			
			$config['upload_path'] = $path;
			$config['allowed_types'] = '*';
			$config['max_size'] = '100000';
			$config['max_width'] = '100024';
			$config['max_height'] = '100768';
			
			$this->load->library('upload', $config);
			$this->upload->do_upload('images');
			$datas = $this->upload->data();
			$name_array = $datas['file_name'];
			$postdata['imageName'] = $name_array;
			$postdata['editId'] = $_POST['editId'];
			$result = $this->animal->updateProfilePic($postdata);
		}
		else
		{
			$result = $this->animal->updateAnimal($_POST);
		}
		
		if(isset($result['success']))
		{
			$response['success'] = $result['success'];
		}
		else
		{
			$response['success'] = $result['error'];
		}
		echo json_encode($response);exit;
	}

/*===============Export animal profile==============*/

	public function exportAnimalProfile()
	{
		if($_POST['doc_type'] == 'pdf')
		{
			$data['editAnimal'] = $this->animal->getAnimalList($_POST['id']);
			$body = $this->load->view("pdf/animal-profile-pdf",$data,TRUE);
			include_once APPPATH.'/third_party/mpdf/mpdf.php';
			$mpdf = new mPDF('c', 'A4', '', '', 20, 15, 48, 25, 10, 10);
	        $mpdf->SetProtection(array('print'));
	        $mpdf->SetDisplayMode('fullpage');
	        $mpdf->WriteHTML($body);
	        $randomString = $data['editAnimal'][0]->id;
	        $valName = 'Animalprofile-' . $randomString . '.pdf';
	        $mpdf->Output($valName, 'D');  
		}
		else
		{
			header("Content-Type: application/vnd.ms-word");
			header("Expires: 0");
			header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
			header("Content-disposition: attachment; filename=\"Animal Profile.doc\"");
			$data['editAnimal'] = $this->animal->getAnimalList($_POST['id']);
			$output = $this->load->view("pdf/animal-profile-pdf",$data);
		}
	}

/*=================Add data logs=============*/

	public function manageDataLogs()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$param = $_POST;
			$result = $this->animal->masterDatalog($param);
			if(isset($result['success']))
			{
				$response['success'] = $result['success'];
				if(isset($param['reloadUrl']))
				{
					$response['url'] = site_url().$param['reloadUrl'];
				}
				
			}
			else
			{
				$response['success'] = $result['error'];
			}
			echo json_encode($response);exit;
		}
	}


/*================Export master datalog=============*/
	
	public function exportMasterDatalog()
	{
		header("Content-Type: application/vnd.ms-word");
		header("Expires: 0");
		header("Cache-Control:  must-revalidate, post-check=0, pre-check=0");
		header("Content-disposition: attachment; filename=\"Master DataLog.doc\"");
		$data['datalog'] = $this->user->getMasterDatalog();
		$output = $this->load->view("export-master-datalog",$data);
	}

	public function animalListSorting()
	{
		$data['animal'] = $this->animal->getAnimalListBySort($_POST);
		$this->load->view('manage-animal-sorting',$data);
	}
	


}

