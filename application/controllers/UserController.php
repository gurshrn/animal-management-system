<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

/*=============login page functionality============*/

	public function index()
	{
		
		$this->load->view('landing');
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$results = $this->user->loginUser($_POST);

			$ip=$_SERVER['REMOTE_ADDR'];

			$checkip = $this->user->checkIp($ip);
			if(count($checkip) > 0)
			{
				$currentTime = date('Y-m-d H:i:s');
				$newtimestamp = date('Y-m-d H:i:s',strtotime('+5 minutes',strtotime($checkip->last_login_time)));
			}
			
			
			if(count($checkip) > 0 && $currentTime <= $newtimestamp && $checkip->log_count == 3)  
			{
			    $successMsg = 'Account blocked for 5 min';
				$this->commonMessages($successMsg,false,'');
			}
			else
			{

			if (count($results) > 0) 
			{
				$time = new DateTime($results->last_login_time);
				$diff = $time->diff(new DateTime());
				$minutes = ($diff->days * 24 * 60) +
				           ($diff->h * 60) + $diff->i;
				$logCount = getSettings('log_count');
				$blockTime = getSettings('block_time');
				if($results->log_count >= $logCount->value  && $minutes <= $blockTime->value)
				{
					
				

					$successMsg = 'Account blocked for 5 min';
					$success = false;
					$url = '';
					$this->commonMessages($successMsg,$success,$url);
				}
				else
				{
					$this->session->set_userdata('userId', $results->id);
		            $this->session->set_userdata('name', $results->name);
		            $this->session->set_userdata('role', $results->role);
					
					if($_POST['animal_code'] == '')
					{
						$message = 'Login successfully';
						$userId = $results->id;
						$tableId = $results->id;
						$tablename = 'user';
						$this->addAnimalDatalog($message,$userId,$tableId,$tablename);

						$successMsg = 'Login successfully';
						$success = true;
						if($results->role == 'user')
			            {
			            	$url = site_url('animal-profile');
			            }
			            else
			            {
			            	$url = site_url('dashboard');
			            }
						$this->commonMessages($successMsg,$success,$url);

					}
					else
					{

						
						$animalData = $this->user->checkAnimalCode($_POST);

						if (count($animalData) > 0)
						{
							if($animalData->location_id == $results->location_id && $results->role != 'superadmin')
							{
								$message = 'Login successfully';
								$userId = $results->id;
								$tableId = $results->id;
								$tablename = 'user';
								$this->addAnimalDatalog($message,$userId,$tableId,$tablename);

								$successMsg = 'Login successfully';
								$success = true;
								$url = 'edit-animal/'.$animalData->id;
								$this->commonMessages($successMsg,$success,$url);
							}
							else if($results->role == 'superadmin')
							{
								$message = 'Login successfully';
								$userId = $results->id;
								$tableId = $results->id;
								$tablename = 'user';
								$this->addAnimalDatalog($message,$userId,$tableId,$tablename);

								$successMsg = 'Login successfully';
								$success = true;
								$url = 'edit-animal/'.$animalData->id;
								$this->commonMessages($successMsg,$success,$url);
							}
							else
							{
								$message = 'Login with invalid animal location';
								$userId = $results->id;
								$tableId = $results->id;
								$tablename = 'user';

								$this->addAnimalDatalog($message,$userId,$tableId,$tablename);
								$this->invalidAnimal($results->last_login_time,$results->log_count,$results->id);
								
								$successMsg = 'Invalid animal location';
								$success = false;
								$url = '';
								$this->commonMessages($successMsg,$success,$url);
							}
						}
						else
						{
							$message = 'Login with invalid animal code';
							$userId = $results->id;
							$tableId = $results->id;
							$tablename = 'user';
							

							$this->addAnimalDatalog($message,$userId,$tableId,$tablename);
							$this->invalidAnimal($results->last_login_time,$results->log_count,$results->id);

							$successMsg = 'Invalid animal code';
							$success = false;
							$url = '';
							$this->commonMessages($successMsg,$success,$url);
						}

						
					}
					
				}
			}
			else
			{
				$ip=$_SERVER['REMOTE_ADDR'];
				$checkip = $this->user->checkIp($ip);
				if(count($checkip) > 0)
				{
					$logCounts = getSettings('log_count');
					$logCount = $checkip->log_count+1;
					$newtimestamp = date('Y-m-d H:i:s',strtotime('+5 minutes',strtotime($checkip->last_login_time)));
					$currentTime = date('Y-m-d H:i:s');
					if($currentTime >= $newtimestamp)
					{
						$logCount = 1;
						$lastLoginTime = date('Y-m-d H:i:s');
					}
					else
					{
						$logCount = $checkip->log_count+1;
						$lastLoginTime = '';
					}

					$time = new DateTime($checkip->last_login_time);
					$diff = $time->diff(new DateTime());
					$minutes = ($diff->days * 24 * 60) +
					           ($diff->h * 60) + $diff->i;
					$logCounts = getSettings('log_count');
					$blockTime = getSettings('block_time');
					if($checkip->log_count >= $logCounts->value  && $minutes <= $blockTime->value)
					{
						
						$message = 'Account blocked for 5 min for ip '.$ip;
						$userId = $ip;
						$tableId = '';
						$tablename = '';
						$this->addAnimalDatalog($message,$userId,$tableId,$tablename);
						
						$successMsg = 'Account blocked for 5 min';
						$success = false;
						$url = '';
						$this->commonMessages($successMsg,$success,$url);
						
					}
					else
					{
						$message = 'Login with invalid ip '.$ip;
						$userId = $ip;
						$tableId = '';
						$tablename = '';
						$this->addAnimalDatalog($message,$userId,$tableId,$tablename);
						
						$this->user->updateUserLoginAttempt($lastLoginTime,$logCount,$checkip->ip_address);
						
						$successMsg = 'User code is wrong,Please check';
						$success = false;
						$url = '';
						$this->commonMessages($successMsg,$success,$url);
					}
				}
				else
				{
					$message = 'Login with invalid ip '.$ip;
					$userId = $ip;
					$tableId = '';
					$tablename = '';
					$this->addAnimalDatalog($message,$userId,$tableId,$tablename);

					$data = array(
						'ip_address'=> $ip,
						'log_count'=> 1,
						'created_at'=> date('Y-m-d H:i:s'),
						'modified_at'=> date('Y-m-d H:i:s'),
						'last_login_time'=> date('Y-m-d H:i:s'),
					);
					$this->user->addUserLoginAttempt($data);
					
					$successMsg = 'User code is wrong,Please check';
					$success = false;
					$url = '';
					$this->commonMessages($successMsg,$success,$url);
				}
			}
		}
			
		}	
		
	}

	public function invalidAnimal($last_login_time,$log_counts,$id)
	{
		$newtimestamp = date('Y-m-d H:i:s',strtotime('+5 minutes',strtotime($last_login_time)));
		$currentTime = date('Y-m-d H:i:s');
		if($currentTime >= $newtimestamp)
		{
			$logCount = 1;
		}
		else
		{
			$logCount = $log_counts+1;
		}
		$this->user->updateLogCount($logCount,$id);
	}

	public function addAnimalDatalog($message,$userId,$tableId,$tablename)
	{
		$param = array(
			'actionPerform' => $message,
			'tablename' => $tablename,
			'user_id' => $userId,
			'tableId' => $tableId,
		);
		$this->animal->masterDatalog($param);
	}

	public function commonMessages($successMsg,$success,$url)
	{
		$result['success_message'] = $successMsg;
		$result['success'] = $success;
		$result['url'] = $url;
		echo json_encode($result);exit;

	}

/*============dashboard=============*/

	public function dashboard()
	{
		$userId = $this->session->userdata('userId');
		$userRole = $this->session->userdata('role');
		if($userId == '' || $userRole != 'superadmin')
		{
			redirect(site_url('/'));
		}
		else
		{
			$this->load->view('index');
		}
		
	}
	
/*=================add user===========*/

	public function userList()
	{
		$userId = $this->session->userdata('userId');
		$userRole = $this->session->userdata('role');
		if($userId == '' || $userRole != 'superadmin')
		{
			redirect(site_url('/'));
		}
		else
		{
			$data['locationList'] = $this->user->getLocationListForAnimal();
			$this->load->view('manage-user',$data);
			
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				if($_POST['edit-status'] == 'true')
				{
					$results = $this->user->addUser($_POST);
				
					if(isset($results['success']))
					{
						if($results['type'] =='Add')
						{
							$param = array(
								'actionPerform' => 'Add user and exit manage user',
							);
						}
						else
						{
							$param = array(
								'actionPerform' => 'Update user and exit manage user',
							);
						}
						
						$this->animal->masterDatalog($param);
						
						$result['success'] = $results['success'];
						$result['success_message'] = $results['success_msg'];
						$result['url'] = site_url('dashboard');
					}
					else
					{
						$output['success'] = $results['error'];
						$result['error_message'] = $results['error_msg'];
					}
					
				}
				else
				{
					$param = array(
						'actionPerform' => 'Exit manage user',
					);
					$this->animal->masterDatalog($param);
					$result['success'] = true;
					$result['success_message'] = 'Exit manage user';
					$result['url'] = site_url('dashboard');
				}
				echo json_encode($result);exit;	

				
			}
		}
	}

/*================get user list=============*/

	public function getUserList()
	{
		$data = $this->user->getUserList();
		if(isset($data) && !empty($data))
		{
			foreach($data as $val)
			{
				$user[] = array(
					'id'=>$val->id,
					'name'=> $val->name,
					'location_id'=>$val->location_id,
					'user_code'=>$val->user_code,
					'delete_id'=>$val->id
				);
			}
			echo json_encode($user);exit;
		}
	}

/*=============Delete user=============*/

	public function deleteUser()
	{
		$tableName = 'user';
		$data = $this->user->commonDelete($_POST,$tableName);
		if(isset($data['success']))
		{
			$param = array(
				'actionPerform' => 'Delete user',
				'tablename' => '',
				'user_id' => '',
				'tableId' => '',
			);
			$this->animal->masterDatalog($param);
			
			$result['success'] = $data['success'];
			$result['url'] = site_url('dashboard');
		}
		else
		{
			$result['success'] = $data['error'];
		}
		echo json_encode($result);exit;	
	}

/*=============Logout=============*/

	public function logout()
	{
		$this->session->unset_userdata('userId');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('role');
		redirect(site_url('/'));
	}

/*============Add and Manage location==================*/

	public function manageLocation()
	{
		
		$userId = $this->session->userdata('userId');
		$userRole = $this->session->userdata('role');
		if($userId == '' || $userRole != 'superadmin')
		{
			redirect(site_url('/'));
		}
		else
		{
			if ($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$data = $this->user->addLocation($_POST['group']);
				if(isset($data['success']))
				{

					if(isset($data['lastId']))
					{
						$result['lastId'] = $data['lastId'];
					}
					$result['success'] = true;
				}
				else
				{
					$result['success'] = false;
				}
				echo json_encode($result);exit;
			}
			$this->load->view('manage-location');
		}
	}

/*===============manage location email==============*/

	public function manageLocationEmail()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$data = $this->user->addLocationEmails($_POST);
			if(isset($data['success']))
			{
					
					
				$result['success'] = true;
				$result['success_msg'] = 'Email added successfully';
			}
			else
			{
				$result['success'] = false;
				$result['success_msg'] = 'Email not added successfully';
			}
			
			echo json_encode($result);exit;
		}
	}

/*=================get email list with location id===============*/

	public function emailList()
	{
		$locationId = $_POST['location_id'];
		$data = $this->user->getLocationEmailList($locationId,'email');
		if(!empty($data))
		{
			foreach($data as $val)
			{
				$result[] = $val->email;
			}
			echo implode("\n",$result);
		}
		else
		{
			echo 0;
		}
	}

/*=================get location list==============*/

	public function locationList()
	{
		$data = $this->user->getLocationList();
		if(isset($data) && !empty($data))
		{
			foreach($data as $val)
			{
				$location[] = array(
					'id'=>$val->id,
					'name'=> $val->location_name,
					'delete_id'=>$val->id
				);
			}
			echo json_encode($location);exit;
		}
	}

/*============delete manage location===========*/

	public function deleteManageLocation()
	{
		$tableName = 'manage_location';
		$data = $this->user->commonDelete($_POST,$tableName);
		if(isset($data['success']))
		{
			$param = array(
				'actionPerform' => 'Delete location',
				'tablename' => '',
				'user_id' => '',
				'tableId' => '',
			);
			$this->animal->masterDatalog($param);
			$result['success'] = $data['success'];
		}
		else
		{
			$result['success'] = $data['error'];
		}
		echo json_encode($result);exit;	
	}

/*===========get master datalog list===========*/

	public function getMasterLog()
	{
		$userId = $this->session->userdata('userId');
		$userRole = $this->session->userdata('role');
		if($userId == '' || $userRole != 'superadmin')
		{
			redirect(site_url('/'));
		}
		else
		{
			$data['datalog'] = $this->user->getMasterDatalog();
			$this->load->view('master-datalog',$data);
		}
	}

	public function checkUserCode()
	{
		$usercode = $_POST['usercode'];
		$result = $this->user->getUserDetail($usercode);
		echo count($result);

	}
	public function checkLocationName()
	{
		$result = $this->user->checkName($_POST);
		if(count($result) == 0)
		{
			$success = true;
		}
		else
		{
			$success = false;
		}
		echo json_encode($success);exit;
		
	}


}
