<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class CronController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function secondInjectionCron()
	{
		$current_time = date('H:i');
		//if($current_time == '00:00')
		//{
			$data = $this->cron->getAnimalList('secondInj');
			
			
			if(count($data)>0)
			{
				$subject = 'Second Injection pending';
				
				foreach($data as $val)
				{
					$locationEmail = getAllAssociatedEmails($val->location_id);
					if(!empty($locationEmail))
					{
					    $message = 'Second Injection pending for animal code :'.$val->animal_code;
						$variables = array(
                            'subject' => $subject,
                            'message' => $message,
                            'emailto' => $locationEmail,
                        );
                       

                        sendEmail($variables);
					}
					
				}	
			}
		//}
	}
	public function profileImageCron()
	{
		$current_time = date('H:i');
		//if($current_time == '00:00')
		//{
			$data = $this->cron->getAnimalList('profile');
			if(count($data)>0)
			{
				$subject = 'Need to upload profile image';
				foreach($data as $val)
				{
					$locationEmail = getAllAssociatedEmails($val->location_id);
					if(!empty($locationEmail))
					{
						$message = 'Need to upload profile image for animal code :'.$val->animal_code;
						$variables = array(
                            'subject' => $subject,
                            'message' => $message,
                            'emailto' => $locationEmail,
                        );

                        sendEmail($variables);
					}
				}	
			}
		//}
	}
	public function weightObsCron()
	{
		$current_time = date('H:i');
		//if($current_time == '18:01')
		//{
			$data = $this->cron->getWeightObsList();
			if(count($data)>0)
			{
				$subject = 'Need to enter Weight';
				foreach($data as $val)
				{
					$locationId = $this->cron->getLocationId($val->animal_id);
					$locationEmail = getAllAssociatedEmails($locationId->location_id);
					if(!empty($locationEmail))
					{
						$animalWeightList = $this->cron->getAnimalWeighttList($val->animal_id);
						
						$totalCount = $val->obs_count-1;
						$updateCount = $this->cron->updateCount($totalCount,$val->animal_id);
						if(count($animalWeightList)==0)
						{
						    
							$message = 'Need to enter Weight for animal code :'.$locationId->animal_code;
							$variables = array(
                                'subject' => $subject,
                                'message' => $message,
                                'emailto' => $locationEmail,
                            );
    
                            sendEmail($variables);
						}
					}
				}
			}

		//}
	}
	public function cron()
	{
		$this->secondInjectionCron();
		$this->profileImageCron();
	}


}